# lumicademy ![npm version](https://img.shields.io/badge/dynamic/json?label=npm&prefix=v&query=version&url=https%3A%2F%2Fraw.githubusercontent.com%2Fjoshdep%2Flumicademy%2Fmaster%2Fpackage.json) ![install size](https://packagephobia.now.sh/badge?p=lumicademy)

A library of functions for using the Lumicademy APIs.

## Install

```
$ npm install lumicademy
```

## Importing

The entire library is modular. You can import the whole library, a specific set of APIs, or even just a function from a module.

```js
// Imports the entire lumicademy library
import Lumi from 'lumicademy';

let conferences = await Lumi.core.conferences.get();


// EX: Import the Core API
import Core from 'lumicademy/apis/core';

let conferences = await Core.conferences.get();


// EX: Import 'getConferences' from Conference Core API
import { getConferences } from 'lumicademy/core/conference';

let conferences = await getConferences();
```

## Usage

The examples below will breakdown each function from the API modules.<br />
For addtional details, see the [API Documentation](https://developers.lumicademy.com/Docs/apis)

> **NOTE:** All APIs that are `async` will support both `promises` and `callback` functions.<br />
> To use a callback, include it as your last argument. All callbacks should be formatted as:<br />
> `(err, data) => {}`

### Auth API

```js
// Import the Auth APIs
import Auth from 'lumicademy/apis/auth';

/* AUTHENTICATION */

// Sign in with Lumicademy credentials
// NOTE: access_token will automatically be cached for usage in other APIs
let tokenData = await Auth.signin({
	username: '[LUMI_USERNAME]',
	password: '[LUMI_PASSWORD]'
});

// Removes either provided access_token or cached token and signs out
let result = await Auth.signout(access_token);

/* TOKENS */

// Returns the currently cached access_token
const access_token = Auth.token.get();

// Returns the currently cached refresh_token
const refresh_token = Auth.token.getRefresh();

// Updates the cached token info
// NOTE: signin and refreshToken functions handle this automatically
Auth.token.set(tokenInfo);

// Removes cached token info
// NOTE: signout automatically calls this function
Auth.token.remove();

// Add a listener for changes to cached token info
Auth.token.addListener(listener);

// Remove a previously added listener for token info changes
Auth.token.removeListener(listener);

// Example listener function
const onTokenChange = isValid => {
	// Token was updated / removed or is no longer valid
	// Handle change here
};

// Returns token info about either provided token or cached token
let tokenInfo = await Auth.token.getInfo(access_token);

// Refreshs the cached token with either provided refresh_token
// or cached refresh token
let tokenData = await Auth.token.refresh(refresh_token);
````

### Core API

```js
// Import the Core APIs
import Core from 'lumicademy/apis/core';

/* CONFERENCES */

// Create a new conference
// See API documentation for conference data options
let newConference = await Core.conference.new(confData);

// Get all details about a specific conference
let conference = await Core.conference.get(confId);

// Update conference details
let updatedConference = await Core.conference.update(confId, changes);

// Delete a conference
// NOTE: Conference state must be ended (3) to delete it
let result = await Core.conference.delete(confId);

// Get all linked content for given conference
let contents = await Core.conference.content.get(confId);

// Link content to a conference
// Optional relatedAs: 1 for document share or 2 for file share. Default 1
let linkedContent = await Core.conference.content.link(confId, contentId, relatedAs);

// Unlink content from a conference
let unlinkedContent = await Core.conference.content.unlink(confId, contentId);

// Get all conference with optional filters
// See API documentation for filters
let conferences = await Core.conferences.get(filters);


/* USERS */

// Create a new user in given conference
// See API documentation for user data
let newUser = await Core.user.new(confId, userData);

// Get all details about a  specific user
let user = await Core.user.get(confId, userId);

// Update a user
let updatedUser = await Core.user.update(confId, userId, changes);

// Change a user's role and privileges
let updatedUser = await Core.user.changeRole(confId, userId, role);

// Delete a user
let result = await Core.user.delete(confId, userId);

// Move an active user to a different conference or a new one
let result = await Core.user.move(confId, userId, confDetails);

// Expel an active user from a conference
let result = await Core.user.expel(confId, userId);

// Get all users matching optional filters
// See API documentation for filter options
let users = await Core.users.get(confId, filters);

// Get total number of active users
let total = await Core.users.getTotal(confId);

// Updates either specified userIds or userGroup
// User Groups: all, attendees, presenters, hosts
let result = await Core.users.update(confId, { userIds, userGroup }, changes);

// Updates role for either specified userIds or userGroup
let result = await Core.users.changeRole(confId, { userIds, userGroup }, role);

// Moves either specified userIds or userGroup
let result = await Core.users.move(confId, { userIds, userGroup }, confDetails);

// Expels either specified userIds or userGroup
let result = await Core.users.expel(confId, { userIds, userGroup });


/* RECORDINGS */

// Returns all details of specific recording
let recording = await Core.recording.get(confId, recordingId);

// Returns a share link with provided settings
// See API documentation for settings options
let shareUrl = await Core.recording.share(confId, recordingId, settings);

// Returns a download link with no expiration
let downloadUrl = await Core.recording.download(confId, recordingId, filename);

// Update the name of specified recording
let updatedRecording = await Core.recording.updateName(confId, recordingId, displayName);

// Delete a recording
let result = await Core.recording.delete(confId, recordingId);

// Get all recordings
let recordings = await Core.recordings.get(confId);


/* TOAST */

// Sends a notification to the client app with provided settings
// See API documentation for toast data options
let result = Core.toast.send(confId, toastData);
```

### Content API

```js
// Import the Content APIs
import ContentAPI from 'lumicademy/apis/content';
import Content from 'lumicademy/apis/content/content-model';

// Uploads a file from formData with optional progress callback
// NOTE: Use included content model to easily convert files to support form data
let newContent = await ContentAPI.content.new(contentData, onUploadProgress);

// Convert a file from file input to supported form data;
let content = Content.fromFile(file);
let contentData = content.getFormData();

// Download specified content with optional progress callback
let content = await ContentAPI.content.get(contentId, onDownloadProgress);

// Update a specific file with optional progress callback
let updatedContent = await ContentAPI.content.update(contentId, contentData, onUploadProgress);

// Deletes a file from content library
// If forceDelete is true, the content will be deleted even if it is linked to a conference
let result = await ContentAPI.content.delete(contentId, forceDelete);

// Gets all stored content
let contents = await ContentAPI.contents.get();
```

## Navigating Directories

There are three different directories within this project:
- `apis`: The main directory used when importing the project.
	> This directory is auto-generated from `apis-ts` using the `npm run build` command.
- `apis-ts`: The TypeScript version of the main project.
	> Make updates and continue development in this directory then rebuild the JavaScript using the command: `npm run build`.
- `apis-no-types`: **Deprecated** - The original set of APIs written in JavaScript. 
	> **NOTE**: Do not continue development in this directory. 
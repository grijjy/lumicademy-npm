import { auth, promiseCallback, Cache } from '../config.js';

// TOKEN MANAGEMENT

export const TOKEN_KEY = `lumi_access_token`;
export const REFRESH_TOKEN = `lumi_refresh_token`;
export const TOKEN_IS_VALID = `lumi_token_is_valid`;

const VALID = true, INVALID = false;

/*
 * TOKEN TIMER
 * Manages token expiration and attempts to refresh once expired
 */
var TokenTimer = {
    timer: null,
    setup: function(tokenInfo) {
        var expires = tokenInfo.expires_in - 60; // In seconds 
        this.timer = setTimeout(() => {
            refreshToken().catch(err => {
                Token.notifyListeners(INVALID);
            });
        }, expires * 1000);
    },
    start: function(tokenInfo) {
        if (this.timer) this.stop();
        if (tokenInfo === undefined) {
            getTokenInfo()
                .then(this.setup)
                .catch(err => console.log(err));
        } else {
            this.setup(tokenInfo);
        }
    },
    stop: function() {
        clearTimeout(this.timer);
    }
};

/*
 * TOKEN
 * Object to store, retrieve, and add listeners to token
 */
export var Token = {
    get: () => Cache.get(TOKEN_KEY),
    getRefresh: () => Cache.get(REFRESH_TOKEN),
    isValid: () => JSON.parse(Cache.get(TOKEN_IS_VALID) ?? false),
    set: function(tokenInfo) {
        if (tokenInfo === undefined) return;
        Cache.set(TOKEN_KEY, tokenInfo.access_token);
        Cache.set(REFRESH_TOKEN, tokenInfo.refresh_token);
        TokenTimer.start(tokenInfo);
        setTimeout(() => this.notifyListeners(VALID), 1);
    },
    remove: () => {
        localStorage.removeItem(TOKEN_KEY);
        localStorage.removeItem(REFRESH_TOKEN);
        localStorage.removeItem(TOKEN_IS_VALID);
        TokenTimer.stop();
    },
    listeners: [],
    addListener: function(listener) {
        this.listeners.push(listener);
    },
    removeListener: function(listener) {
        var index = this.listeners.indexOf(listener);
        if (index !== -1) this.listeners.splice(index, 1);
    },
    notifyListeners: function(isValid) {
        let validState = isValid ?? INVALID;
        Cache.set(TOKEN_IS_VALID, validState);
        this.listeners.forEach(listener => {
            listener(validState);
        });
    }
};

/*
 * CHECK FOR AUTH ERROR
 * Checks any error handler for possible authorization issues with Lumicademy.
 * If token is invalid, the token observer is called to notify all subscribers.
 */
export const checkForAuthError = err => {
    let { status, data } = err?.response ?? { status: 400, data: { error_code: 0 } },
        code = data?.error_code;
    
    if (status === 401 || code === 14) { // 14: TOKEN_UNAUTHORIZED
        Token.notifyListeners(INVALID);
    }
};

// AUTH FUNCTIONS

/*
 * SIGN IN
 * Returns token info from given Lumicademy credentials
 */
export const signin = ({ username, password }, callback) => {
    return promiseCallback(async () => {
        try {
            if (username === undefined || password === undefined) throw new Error("Missing agruments");
            let { data } = await auth().post('/auth/login', { username, password });
            Token.set(data);
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

/*
 * SIGN OUT
 * Removes and invalidates the stored token
 */
export const signout = (access_token, callback) => {
    return promiseCallback(async () => {
        try {
            access_token = access_token ?? Token.get();
            let { data } = await auth(access_token).delete('/auth/logout');
            Token.remove();
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

/*
 * GET TOKEN FROM CLIENT CREDENTIALS
 * Each app under an account has it's own client credentials. Use this function to authorize
 * access to one app instead of an entire account. Find client credentials under the app settings page.
 */
export const getTokenFromClientCredentials = ({ client_id, client_secret }, callback) => {
    return promiseCallback(async () => {
        try {
            if (client_id === undefined || client_secret === undefined) throw new Error("Missing arguments");
            let { data } = await auth().post('/auth/oauth2/token',
                `grant_type=client_credentials&client_id=${client_id}&client_secret=${client_secret}`
            );
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

/*
 * GET TOKEN INFO
 * Returns details information about the given access_token
 */
export const getTokenInfo = (token, callback) => {
    token = token ?? Token.get();
    return promiseCallback(async () => {
        try {
            if (token === undefined) throw new Error("Missing token");
            let options = { params: { 'access_token': token } };
            let { data } = await auth().get('/auth/oauth2/tokenInfo', options);
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

/*
 * REFRESH TOKEN
 * Allows the access_token to be refreshed with provided refresh_token
 */
export const refreshToken = (refresh_token, callback) => {
    refresh_token = refresh_token ?? Token.getRefresh();
    return promiseCallback(async () => {
        try {
            if (refresh_token === undefined) throw new Error("Missing token");
            let { data } = await auth().post('/auth/oauth2/token', {
                "grant_type": "refresh_token",
                refresh_token
            });
            Token.set(data);
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

export default {
    signin,
    signout,
    token: {
        ...Token,
        fromClientCredentials: getTokenFromClientCredentials,
        getInfo: getTokenInfo,
        refresh: refreshToken
    }
};
import axios from 'axios';
import NodeCache from 'node-cache';

const cache = new NodeCache();

// REQUEST OPTIONS AND SETTINGS

const defaultHeaders = {
    'accept': 'application/json',
    'content-type': 'application/json'
};

export const allDetails = {
    params: {
        'details': 'all'
    }
};

export const summary = {
    params: {
        'details': 'summary'
    }
};

export const fileOptions = uploadCallback => ({
    headers: {},
    onUploadProgress: uploadCallback
});

export const imgOptions = uploadCallback => ({
    headers: {
        'content-type': 'image/*'
    },
    onUploadProgress: uploadCallback
});

export const downloadOptions = downloadCallback => ({
    responseType: 'blob',
    onDownloadProgress: downloadCallback
});

// REQUEST FUNCTIONS

export const api = token => {
    let headers = { ...defaultHeaders };
    if (token) {
        headers['Authorization'] = `Bearer ${token}`;
    }
    return axios.create({
        baseURL: 'https://api.lumicademy.com',
        headers
    });
};

export const upload = token => {
    let headers = token
        ? { 'Authorization': `Bearer ${token}` }
        : undefined;
    return axios.create({
        baseURL: 'https://api.lumicademy.com',
        headers
    });
};

export const auth = token => {
    let headers = { ...defaultHeaders };
    if (token) {
        headers['Authorization'] = `Bearer ${token}`;
    }
    return axios.create({
        baseURL: 'https://auth.lumicademy.com',
        headers
    });
};

// MISC HELPER FUNCTIONS

// Wraps an promise to handle and optional callback
export const promiseCallback = (action, callback) => {
    if (callback) {
        return action()
            .then(result => callback(null, result))
            .catch(err => callback(err, null));
    } else {
        return action();
    }
};

// Attempts to cache in localStorage and falls back to
// node-cache if not running in a browser
export const Cache = {
    get: key => {
        try {
            return localStorage.getItem(key);
        } catch (e) {
            return cache.get(key);
        }
    },
    set: (key, value) => {
        try {
            localStorage.setItem(key, value);
        } catch (e) {
            cache.set(key, value);
        }
    },
    remove: key => {
        try {
            return localStorage.removeItem(key);
        } catch (e) {
            return cache.del(key);
        }
    }
};

export default {
    allDetails,
    summary,
    fileOptions,
    imgOptions,
    downloadOptions,
    api,
    upload,
    auth,
    promiseCallback,
    Cache
};
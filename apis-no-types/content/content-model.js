/*
 * DELETE METHOD
 * Valid values for content delete method type
 */
export const DeleteMethod = { NEVER: 1, AUTO: 2 };

/*
 * CONTENT
 * A class for managing Lumicademy content types
 */
export class Content {
    constructor(content) {
        this.contentId = content?.contentId ?? undefined;
        this.content = content?.content ?? content ?? "";
        this.contentType = content?.contentType ?? content?.type ?? "*/*";
        this.displayName = content?.displayName ?? content?.name ?? "";
        this.fileName = content?.fileName ?? content?.name ?? "";
        this.created = content?.created ?? undefined;
        this.deleteMethod = content?.deleteMethod ?? DeleteMethod.NEVER;
    }

    // Creates a content object from an HTML File API object
    static fromFile = file => {
        return new this(file);
    };

    // Creates the required form data object from the content
    getFormData = () => {
        const request = {
            contentType: this.contentType,
            displayName: this.displayName,
            fileName: this.fileName,
            deleteMethod: this.deleteMethod
        };

        const json = JSON.stringify(request);
        const requestBlob = new Blob([json], { type: "application/json" });

        let data = new FormData();
        data.append('request', requestBlob);
        data.append('content', this.content);
        return data;
    };
    
};

export default Content;
import { api, upload, fileOptions, downloadOptions, promiseCallback } from '../config.js';
import { Token, checkForAuthError } from '../auth/index.js';

/*
 * NEW CONTENT
 * Takes contentData as a FormData object with optional onUploadProgress callback
 */
export const newContent = (contentData, onUploadProgress, callback) => {
    return promiseCallback(async () => {
        try {
            let { data } = await upload(Token.get()).post("/content", contentData, fileOptions(onUploadProgress));
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET CONTENT
 * Downloads the content with given contentId.
 * Also has optional onDownloadProgress callback
 */
export const getContent = (contentId, onDownloadProgress, callback) => {
    return promiseCallback(async () => {
        try {
            if (contentId === undefined) throw new Error("Missing contentId");
            let { data } = await api(Token.get()).get(`/content/${contentId}`, downloadOptions(onDownloadProgress));
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * UPDATE CONTENT
 * Updates the content info or content itself using the same FormData structure.
 * onUploadProgress is optional
 */
export const updateContent = (contentId, contentData, onUploadProgress, callback) => {
    return promiseCallback(async () => {
        try {
            if (contentId === undefined) throw new Error("Missing contentId");
            let { data } = await upload(Token.get()).put(`/content/${contentId}`, contentData, fileOptions(onUploadProgress));
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * DELETE CONTENT
 * Deletes content with given contentId.
 * forceDelete will force the deletion even if the content is linked to a conference.
 */
export const deleteContent = (contentId, forceDelete, callback) => {
    return promiseCallback(async () => {
        try {
            if (contentId === undefined) throw new Error("Missing contentId");
            let options = forceDelete ? { params: { force: forceDelete } } : {};
            let { data } = await api(Token.get()).delete(`/content/${contentId}`, options);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET CONTENTS
 * Returns all contents associated with account
 */
export const getContents = (callback) => {
    return promiseCallback(async () => {
        try {
            let { data } = await api(Token.get()).get("/contents");
            return data.items;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

export default {
    content: {
        new: newContent,
        get: getContent,
        update: updateContent,
        delete: deleteContent
    },
    contents: {
        get: getContents
    }
};
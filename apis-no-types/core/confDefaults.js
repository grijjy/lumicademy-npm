export const STATE = { WAITING: 1, STARTED: 2, ENDED: 3, DELETED: 4 };

export const features = {
    video: true,
    audio: true,
    whiteboard: true,
    desktopShare: true,
    documentShare: true,
    fileShare: true,
    recording: true,
    chat: true
};

export const privileges = {
    annotate: true,
    video: true,
    audio: true,
    share: false,
    viewUsers: true,
    record: false,
    changePages: false,
    changeShares: false,
    chat: true,
    changeUser: false
};

export const presenterPrivileges = {
    annotate: true,
    video: true,
    audio: true,
    share: true,
    viewUsers: true,
    record: false,
    changePages: true,
    changeShares: true,
    chat: true,
    changeUser: true
};

export const hostPrivileges = {
    annotate: true,
    video: true,
    audio: true,
    share: true,
    viewUsers: true,
    record: true,
    changePages: true,
    changeShares: true,
    chat: true,
    changeUser: true
};

export default {
    displayName: "",
    startMethod: 2,
    backgroundColor: 0,
    location: 1,
    countryCode: "",
    features,
    privileges,
    presenterPrivileges,
    hostPrivileges,
    attendMethods: {
        password: true,
        url: true
    }
};
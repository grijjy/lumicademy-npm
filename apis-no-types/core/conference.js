import { api, promiseCallback, allDetails } from '../config.js';
import { Token, checkForAuthError } from '../auth/index.js';
import confDefaults from './confDefaults.js';

/*
 * NEW CONFERENCE
 * Creates a new conference with all default settings and specified confData
 */
export const newConference = (confData, callback) => {
    return promiseCallback(async () => {
        try {
            let { data } = await api(Token.get()).post('/conference', {
                ...confDefaults, ...confData
            });
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET CONFERENCE
 * Retrieves all details about a given conference
 */
export const getConference = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let { data } = await api(Token.get()).get(`/conference/${confId}`, allDetails);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * UPDATE CONFERENCE
 * Updates a specified conference with given changes
 */
export const updateConference = (confId, changes, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || changes === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).put(`/conference/${confId}`, changes);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * DELETE CONFERENCE
 * Deletes a conference.
 * Note: A conference can only be deleted if its state is ended (3);
 */
export const deleteConference = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let { data } = await api(Token.get()).delete(`/conference/${confId}`);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET LINKED CONTENT
 * Returns a list of all content currently linked to a given conference
 */
export const getLinkedContent = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let { data } = await api(Token.get()).get(`/conference/${confId}/contents`, allDetails);
            return data.items;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * LINK CONTENT
 * Links content to a specified conference
 */
export const linkContent = (confId, contentId, relatedAs, callback) => {
    relatedAs = relatedAs ?? 1; // 1: DOCSHARE, 2: FILESHARE
    return promiseCallback(async () => {
        try {
            if (confId === undefined || contentId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).post(`/conference/${confId}/content/${contentId}`, { relatedAs });
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * UNLINK CONTENT
 * Removes a link between a conference and content. Content is not deleted
 */
export const unlinkContent = (confId, contentId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || contentId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).delete(`/conference/${confId}/content/${contentId}`);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET CONFERENCES
 * Returns a list of all conferences with optional filters
 */
export const getConferences = (filters, callback) => {
    return promiseCallback(async () => {
        try {
            let options = { params: { details: "all" } };
            if (filters) {
                Object.keys(filters).forEach(key => {
                    options.params[key] = filters[key] ? filters[key] : undefined;
                });
            }
            let { data } = await api(Token.get()).get('/conferences', options);
            return data.items;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

export default {
    conference: {
        new: newConference,
        get: getConference,
        update: updateConference,
        delete: deleteConference,
        content: {
            get: getLinkedContent,
            link: linkContent,
            unlink: unlinkContent
        }
    },
    conferences: {
        get: getConferences
    }
};
import ConferenceAPIs from './conference.js';
import UserAPIs from './user.js';
import RecordingAPIs from './recording.js';
import ToastAPIs from './toast.js';

export default {
    ...ConferenceAPIs,
    ...UserAPIs,
    ...RecordingAPIs,
    ...ToastAPIs
};
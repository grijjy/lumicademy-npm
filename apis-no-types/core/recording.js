import { api, promiseCallback, allDetails } from '../config.js';
import { Token, checkForAuthError } from '../auth/index.js';

/*
 * GET RECORDING
 * Return details about given recording
 */
export const getRecording = (confId, recordingId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || recordingId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).get(`/conference/${confId}/recording/${recordingId}`, allDetails);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET RECORDINGS
 * Returns a list of all recordings of specified conference
 */
export const getRecordings = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).get(`/conference/${confId}/recordings`, allDetails);
            return data.items;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET SHARE URL
 * Returns a shareable url to view recording
 * Optional settings: expiry_minutes, expiry, content_disposition, content_type
 * NOTE: See getDownload for example of using settings
 */
export const getShareUrl = (confId, recordingId, settings, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || recordingId === undefined) throw new Error("Missing arguments");
            let options = {
                params: {
                    details: "all",
                    ...settings
                }
            };
            let { data } = await api(Token.get()).get(`/conference/${confId}/recording/${recordingId}/sharedaccess`, options);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET DOWNLOAD
 * A specific implementation of getShareUrl to return a download link with no expiration
 */
export const getDownload = (confId, recordingId, filename, callback) => {
    return promiseCallback(async () => {
        try {
            let settings = {
                content_disposition: `attachment; filename=${filename ?? `Recording_${recordingId}`}.mp4`,
                content_type: "binary"
            };
            let download = await getShareUrl(confId, recordingId, settings, callback);
            return download;
        } catch (e) {
            throw e;
        }
    }, callback);
};

/*
 * UPDATE RECORDING NAME
 * Updates the name of the recording
 */
export const updateRecordingName = (confId, recordingId, displayName, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || recordingId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).put(`/conference/${confId}/recording/${recordingId}`, { displayName });
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * DELETE RECORDING
 * Removes a recording
 */
export const deleteRecording = (confId, recordingId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || recordingId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).delete(`/conference/${confId}/recording/${recordingId}`);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

export default {
    recording: {
        get: getRecording,
        share: getShareUrl,
        download: getDownload,
        updateName: updateRecordingName,
        delete: deleteRecording
    },
    recordings: {
        get: getRecordings
    }
};
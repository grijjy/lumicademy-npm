import { api, promiseCallback } from '../config.js';
import { Token, checkForAuthError } from '../auth/index.js';

export const ToastIcons = {
    None: 0,
    Success: 1,
    Warning: 2,
    Error: 3,
    Info: 4
};

export const ToastSounds = {
    None: 0,
    Chat: 1,
    Join: 2,
    Leave: 3
};

export const UserGroup = {
    Specified: 1,
    All: 2,
    Attendees: 3,
    Presenters: 4,
    Hosts: 5
};

const defaultToast = {
    messageText: "",
    icon: ToastIcons.None,
    sound: ToastSounds.None,
    durationMs: 3000,
    userGroup: UserGroup.Specified,
    userIds: [],
    flags: 0,
    buttonText: undefined,
    buttonUrl: undefined
};

/*
 * SEND TOAST
 * Sends a notification to the client app with provided settings
 * See defaultToast for options
 */
export const sendToast = (confId, toastData, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || toastData.messageText === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).post(`/conference/${confId}/toast`, {
                ...defaultToast,
                ...toastData
            });
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

export default {
    toast: {
        send: sendToast
    }
};
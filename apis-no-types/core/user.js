import { api, promiseCallback, allDetails } from '../config.js';
import { privileges as privilegesDefaults } from './confDefaults.js';
import { Token, checkForAuthError } from '../auth/index.js';

/*
 * GET PRIVILEGES
 * Returns the default privileges for a given role from a conference
 */
export const getPrivileges = (confId, role, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            role = role ?? 1;
            let { data } = await api(Token.get()).get(`/conference/${confId}`, allDetails);
            let lookupKey = { 1: "privileges", 2: "presenterPrivileges", 3: "hostPrivileges" };
            let privileges = data[lookupKey[role]];
            if (privileges === undefined) {
                privileges = { ...privilegesDefaults };
                switch (role) {
                    case 1:
                        break;
                    case 2:
                    case 3:
                        Object.keys(privilegesDefaults).forEach(privilegeKey => {
                            privileges[privilegeKey] = (privilegeKey === "record" && role === 2) ? false : true;
                        });
                        break;
                    default:
                        break;
                }
            }
            return privileges;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * NEW USER
 * Creates a new user from provided userData
 */
export const newUser = (confId, userData, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userData === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).post(`/conference/${confId}/user`, userData);
            if (userData?.role !== undefined && userData?.privileges === undefined) {
                let privileges = await getPrivileges(confId, userData.role);
                return await updateUser(confId, data.userId, { privileges });
            }
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET USER
 * Returns details about a given user
 */
export const getUser = (confId, userId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).get(`/conference/${confId}/user/${userId}`, allDetails);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * UPDATE USER
 * Updates a user with provided changes
 */
export const updateUser = (confId, userId, changes, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).put(`/conference/${confId}/user/${userId}`, changes);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * CHANGES USER ROLE
 * A quick function to change a user's role and associated privileges
 */
export const changeUserRole = (confId, userId, role, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined) throw new Error("Missing arguments");
            role = role ?? 1;
            let privileges = await getPrivileges(confId, role),
                user = await updateUser(confId, userId, { role, privileges });
            user.role = role;
            user.privileges = privileges;
            return user;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * DELETE USER
 * Deletes a user from the conference
 */
export const deleteUser = (confId, userId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).delete(`/conference/${confId}/user/${userId}`);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * MOVE USER
 * Moves the specified user to either an existing or a new conference.
 * EXISTING: confDetails.id is required and confDetails.password is optional
 * NEW: confDetails will be the details of the new conference. displayName is required
 */
export const moveUser = (confId, userId, confDetails, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined) throw new Error("Missing arguments");
            let changes = {}, options = { params: {} };
            if (confDetails.id) {
                options.params.id = confDetails.id;
                if (confDetails.password) options.params.password = confDetails.password;
            } else {
                options.params.new = true;
                changes = { ...confDetails };
            }
            let { data } = await api(Token.get()).put(`/conference/${confId}/user/${userId}/move`, changes, options);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * EXPEL USER
 * Removes a user from an active conference, but doesn't delete them
 */
export const expelUser = (confId, userId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).delete(`/conference/${confId}/user/${userId}/expel`);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);   
};

/*
 * GET USERS
 * Returns all users from a conference with optional filters for details to be returned
 * EX: 'all_audio', 'all_video', and 'active' would return all audio and video details for only active users
 */
export const getUsers = (confId, filters, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let options = { params: { details: "all" } };
            if (filters) {
                filters.forEach(filter => {
                    options.params[filter] = true;
                });
            }
            let { data } = await api(Token.get()).get(`/conference/${confId}/users`, options);
            return data.items;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * GET TOTAL ACTIVE USERS
 * Returns the number of users that are currently active in a conference.
 */
export const getTotalActiveUsers = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let options = { params: { active: true } };
            let { data } = await api(Token.get()).get(`/conference/${confId}/users`, options);
            return data.items.length;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * UPDATE USERS
 * Updates either all specified userIds or userGroup with given changes.
 */
export const updateUsers = (confId, { userIds, userGroup }, changes, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            if (userIds) changes.userIds = userIds;
            let options = userGroup ? { params: { apply_to: userGroup } } : {};
            let { data } = await api(Token.get()).put(`/conference/${confId}/users`, changes, options);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * CHANGE USERS ROLE
 * A quick function to change specified role and associated privileges of given userIds or userGroup 
 */
export const changeUsersRole = (confId, { userIds, userGroup }, role, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || role === undefined) throw new Error("Missing arguments");
            let changes = {
                role,
                privileges: await getPrivileges(confId, role)
            };
            let users = await updateUsers(confId, { userIds, userGroup }, changes);
            return users;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * MOVE USERS
 * Moves the specified users to either an existing or a new conference.
 * EXISTING: confDetails.id is required and confDetails.password is optional
 * NEW: confDetails will be the details of the new conference. displayName is required
 */
export const moveUsers = (confId, { userIds, userGroup }, confDetails, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let changes = userIds ? { userIds } : {},
                options = userGroup ? { params: { apply_to: userGroup } } : { params: {} };

            if (confDetails.id) {
                options.params.id = confDetails.id;
                if (confDetails.password) options.params.password = confDetails.password;
            } else {
                options.params.new = true;
                changes = { ...changes, ...confDetails };
            }
            let { data } = await api(Token.get()).put(`/conference/${confId}/users/move`, changes, options);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/*
 * EXPEL USERS
 * Removes users from an active conference, but doesn't delete them
 */
export const expelUsers = (confId, { userIds, userGroup }, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let changes = userIds ? { userIds } : {},
                options = userGroup ? { params: { apply_to: userGroup } } : {};
            let { data } = await api(Token.get()).put(`/conference/${confId}/users/expel`, changes, options);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

export default {
    user: {
        new: newUser,
        get: getUser,
        update: updateUser,
        changeRole: changeUserRole,
        delete: deleteUser,
        move: moveUser,
        expel: expelUser
    },
    users: {
        get: getUsers,
        getTotal: getTotalActiveUsers,
        update: updateUsers,
        changeRole: changeUsersRole,
        move: moveUsers,
        expel: expelUsers
    }
};
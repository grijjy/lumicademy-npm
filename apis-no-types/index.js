import AuthAPI from './auth/index.js';
import CoreAPI from './core/index.js';
import ContentAPI from './content/index.js';

const Lumi = {
    auth: AuthAPI, 
    core: CoreAPI,
    ...ContentAPI
};
export default Lumi;
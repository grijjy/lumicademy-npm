import { auth, promiseCallback, Cache } from '../config';

// TOKEN MANAGEMENT

export const TOKEN_KEY = `lumi_access_token`;
export const REFRESH_TOKEN = `lumi_refresh_token`;
export const TOKEN_IS_VALID = `lumi_token_is_valid`;

const VALID = true, INVALID = false;

export declare interface TokenInfo {
    access_token: string,
    token_type?: string,
    expires_in: number,
    refresh_token?: number,
    grant_type?: string,
    issuer?: string,
    subject?: string,
    scopes?: string,
    client_id?: string,
    storage_id?: string,
    issued_at?: number,
    expires?: number
};

export declare interface TokenTimerData {
    timer: NodeJS.Timeout | null,
    setup: (tokenInfo: TokenInfo) => void,
    start: (tokenInfo: TokenInfo) => void,
    stop: () => void
};

/*
 * TOKEN TIMER
 * Manages token expiration and attempts to refresh once expired
 */
var TokenTimer: TokenTimerData = {
    timer: null,
    setup: function(tokenInfo: TokenInfo) {
        var expires = tokenInfo.expires_in - 60; // In seconds 
        this.timer = setTimeout(() => {
            refreshToken().catch((_err: any) => {
                Token.notifyListeners(INVALID);
            });
        }, expires * 1000);
    },
    start: function(tokenInfo: TokenInfo) {
        if (this.timer) this.stop();
        if (tokenInfo === undefined) {
            getTokenInfo()
                .then(this.setup)
                .catch((err: any) => console.log(err));
        } else {
            this.setup(tokenInfo);
        }
    },
    stop: function() {
        if (this.timer === null) return;
        clearTimeout(this.timer);
    }
};

export type TokenListener = (tokenValid?: Boolean) => void;

export declare interface TokenClass {
    get: () => unknown,
    getRefresh: () => unknown,
    isValid: () => boolean,
    set: (tokenInfo: TokenInfo) => void,
    remove: () => void,
    listeners: TokenListener[],
    addListener: (listener: TokenListener) => void,
    removeListener: (listener: TokenListener) => void,
    notifyListeners: (isValid: boolean) => void
};

/*
 * TOKEN
 * Object to store, retrieve, and add listeners to token
 */
export var Token:TokenClass = {
    get: () => Cache.get(TOKEN_KEY),
    getRefresh: () => Cache.get(REFRESH_TOKEN),
    isValid: () => {
        let isValid = Cache.get(TOKEN_IS_VALID) as string;
        return JSON.parse(isValid ?? "false")
    },
    set: function(tokenInfo: TokenInfo) {
        if (tokenInfo === undefined) return;
        Cache.set(TOKEN_KEY, tokenInfo.access_token);
        Cache.set(REFRESH_TOKEN, tokenInfo.refresh_token);
        TokenTimer.start(tokenInfo);
        setTimeout(() => this.notifyListeners(VALID), 1);
    },
    remove: () => {
        localStorage.removeItem(TOKEN_KEY);
        localStorage.removeItem(REFRESH_TOKEN);
        localStorage.removeItem(TOKEN_IS_VALID);
        TokenTimer.stop();
    },
    listeners: [],
    addListener: function(listener: TokenListener) {
        this.listeners.push(listener);
    },
    removeListener: function(listener: TokenListener) {
        var index = this.listeners.indexOf(listener);
        if (index !== -1) this.listeners.splice(index, 1);
    },
    notifyListeners: function(isValid: Boolean) {
        let validState = isValid ?? INVALID;
        Cache.set(TOKEN_IS_VALID, validState);
        this.listeners.forEach((listener: TokenListener) => {
            listener(validState);
        });
    }
};

/**
 * Checks any error handler for possible authorization issues with Lumicademy.
 * If token is invalid, the token observer is called to notify all subscribers.
 * @param err The error from an error handler
 */
export const checkForAuthError = (err: any) => {
    let { status, data } = err?.response ?? { status: 400, data: { error_code: 0 } },
        code = data?.error_code;
    
    if (status === 401 || code === 14) { // 14: TOKEN_UNAUTHORIZED
        Token.notifyListeners(INVALID);
    }
};

// AUTH FUNCTIONS

export declare interface SignInProps {
    username: string,
    password: string
};

type SignInCallback = (tokenInfo: TokenInfo) => void;

/**
 * Returns token info from given Lumicademy credentials
 * @param username Lumicademy username
 * @param password Lumicademy password
 * @param callback (Optional) callback function
 * @returns A promise containing {@link TokenInfo}
 */
export const signin = ({ username, password }: SignInProps, callback?: SignInCallback) => {
    return <Promise<TokenInfo>>promiseCallback(async () => {
        try {
            if (username === undefined || password === undefined) throw new Error("Missing agruments");
            let { data } = await auth().post('/auth/login', { username, password });
            Token.set(data);
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

type SignOutCallback = ({ result }: { result: string }) => void;

/**
 * Removes and invalidates the stored token
 * @param access_token (Optional) A specific access_token to log out. Default is stored access_token
 * @param callback (Optional) callback function
 * @returns A promise containing the result
 */
export const signout = (access_token?: string, callback?: SignOutCallback) => {
    return <Promise<{ result: string }>>promiseCallback(async () => {
        try {
            access_token = access_token ?? Token.get() as string;
            let { data } = await auth(access_token).delete('/auth/logout');
            Token.remove();
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

export declare interface ClientCredentials {
    client_id: string,
    client_secret: string
};

type ClientTokenCallback = (tokenInfo: TokenInfo) => void;

/**
 * Each app under an account has it's own client credentials.
 * Use this function to authorize access to one app instead of an entire account.
 * Find client credentials under the app settings {@link https://developers.lumicademy.com/portal/settings}.
 * @param client_id Lumicademy app client id
 * @param client_secret Lumicademy app client secret
 * @param callback (Optional) callback function
 * @returns A promise containing {@link TokenInfo}
 */
export const getTokenFromClientCredentials = ({ client_id, client_secret }: ClientCredentials, callback?: ClientTokenCallback) => {
    return <Promise<TokenInfo>>promiseCallback(async () => {
        try {
            if (client_id === undefined || client_secret === undefined) throw new Error("Missing arguments");
            let { data } = await auth().post('/auth/oauth2/token',
                `grant_type=client_credentials&client_id=${client_id}&client_secret=${client_secret}`
            );
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

type TokenInfoCallback = (tokenInfo: TokenInfo) => void;

/**
 * Returns details information about the given access_token
 * @param token (Optional) Specific access_token to check
 * @param callback (Optional) callback function
 * @returns A promise containing {@link TokenInfo}
 */
export const getTokenInfo = (token?: string, callback?: TokenInfoCallback) => {
    token = token ?? Token.get() as string;
    return <Promise<TokenInfo>>promiseCallback(async () => {
        try {
            if (token === undefined) throw new Error("Missing token");
            let options = { params: { 'access_token': token } };
            let { data } = await auth().get('/auth/oauth2/tokenInfo', options);
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

type RefreshTokenCallback = (tokenInfo: TokenInfo) => void;

/**
 * Allows the access_token to be refreshed with provided refresh_token
 * @param refresh_token (Optional) A specific refresh_token to use
 * @param callback (Optional) callback function
 * @returns A promise containing {@link TokenInfo}
 */
export const refreshToken = (refresh_token?: string, callback?: RefreshTokenCallback) => {
    refresh_token = refresh_token ?? Token.getRefresh() as string;
    return <Promise<TokenInfo>>promiseCallback(async () => {
        try {
            if (refresh_token === undefined) throw new Error("Missing token");
            let { data } = await auth().post('/auth/oauth2/token', {
                "grant_type": "refresh_token",
                refresh_token
            });
            Token.set(data);
            return data;
        } catch (e) {
            throw e;
        }
    }, callback);
};

export default {
    signin,
    signout,
    token: {
        ...Token,
        fromClientCredentials: getTokenFromClientCredentials,
        getInfo: getTokenInfo,
        refresh: refreshToken
    }
};
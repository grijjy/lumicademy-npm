import axios from 'axios';
import NodeCache from 'node-cache';

const cache = new NodeCache();

// REQUEST OPTIONS AND SETTINGS

declare interface Headers {
    accept: string,
    'content-type': string,
    Authorization?: string
};

const defaultHeaders: Headers = {
    'accept': 'application/json',
    'content-type': 'application/json'
};

export const allDetails = {
    params: {
        'details': 'all'
    }
};

export const summary = {
    params: {
        'details': 'summary'
    }
};

export type ProgressCallback = (progressEvent: any) => void;

export const fileOptions = (uploadCallback?: ProgressCallback) => ({
    headers: {},
    onUploadProgress: uploadCallback
});

export const imgOptions = (uploadCallback?: ProgressCallback) => ({
    headers: {
        'content-type': 'image/*'
    },
    onUploadProgress: uploadCallback
});

export const downloadOptions = (downloadCallback?: ProgressCallback) => ({
    responseType: "blob",
    onDownloadProgress: downloadCallback
});

// REQUEST FUNCTIONS

/**
 * Creates a new Axios instance for the Lumi API server with optional token
 * @remarks Uses default headers
 * @param token (Optional) access_token to add to Authorization header
 * @returns Uses {@link axios.create} to return a new AxiosInstance
 */
export const api = (token?: string | unknown) => {
    let headers = { ...defaultHeaders };
    if (token) {
        headers['Authorization'] = `Bearer ${token}`;
    }
    return axios.create({
        baseURL: 'https://api.lumicademy.com',
        headers
    });
};

/**
 * Creates a new Axios instance for the Lumi API server with optional token
 * @remarks Omits default headers for better file handling
 * @param token (Optional) access_token to add to Authorization header
 * @returns Uses {@link axios.create} to return a new AxiosInstance
 */
export const upload = (token?: string | unknown) => {
    let headers = token
        ? { 'Authorization': `Bearer ${token}` }
        : undefined;
    return axios.create({
        baseURL: 'https://api.lumicademy.com',
        headers
    });
};

/**
 * Creates a new Axios instance for the Lumi AUTH server with optional token
 * @remarks Uses default headers
 * @param token (Optional) access_token to add to Authorization header
 * @returns Uses {@link axios.create} to return a new AxiosInstance
 */
export const auth = (token?: string | unknown) => {
    let headers = { ...defaultHeaders };
    if (token) {
        headers['Authorization'] = `Bearer ${token}`;
    }
    return axios.create({
        baseURL: 'https://auth.lumicademy.com',
        headers
    });
};

// MISC HELPER FUNCTIONS

type PromiseAction = () => Promise<any>;
export type PromiseCallback = (err: any, data: any) => void;
export declare interface DeleteResult {
    result: string
};

/**
 * Wraps an promise to handle and optional callback
 * @param action An async action {@link PromiseAction}
 * @param callback (Optional) A generic callback {@link PromiseCallback}
 * @returns Either a promise or void if callback is included
 */
export const promiseCallback = async (action: PromiseAction, callback?: PromiseCallback) => {
    if (callback) {
        try {
            const result = await action();
            return callback(null, result);
        } catch (err) {
            return callback(err, null);
        }
    } else {
        return action();
    }
};

/** 
 * Attempts to use cache in localStorage and falls back to
 * node-cache if not running in a browser
 */
export const Cache = {
    get: (key: string) => {
        try {
            return localStorage.getItem(key);
        } catch (e) {
            return cache.get(key);
        }
    },
    set: (key: string, value: any) => {
        try {
            localStorage.setItem(key, value);
        } catch (e) {
            cache.set(key, value);
        }
    },
    remove: (key: string) => {
        try {
            return localStorage.removeItem(key);
        } catch (e) {
            cache.del(key);
            return;
        }
    }
};

export default {
    allDetails,
    summary,
    fileOptions,
    imgOptions,
    downloadOptions,
    api,
    upload,
    auth,
    promiseCallback,
    Cache
};
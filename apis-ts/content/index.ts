import { AxiosRequestConfig } from "axios";
import { api, upload, fileOptions, downloadOptions, ProgressCallback, promiseCallback, PromiseCallback, DeleteResult } from "../config";
import { ContentData } from "./content-model";
import { Token, checkForAuthError } from '../auth';

/**
 * Takes contentData as a FormData object with optional onUploadProgress callback
 * @param contentData Custom form data for Lumicademy. Use Content.getFormData()
 * @param onUploadProgress (Optional) Progress callback
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ContentData}
 */
export const newContent = (contentData: FormData, onUploadProgress?: ProgressCallback, callback?: PromiseCallback) => {
    return <Promise<ContentData>>promiseCallback(async () => {
        try {
            let { data } = await upload(Token.get()).post("/content", contentData, fileOptions(onUploadProgress));
            return data as ContentData;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Downloads the content with given contentId.
 * Also has optional onDownloadProgress callback
 * @param contentId A Lumicademy content id
 * @param onDownloadProgress (Optional) Progress callback
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link Buffer}
 */
export const getContent = (contentId: string, onDownloadProgress?: ProgressCallback, callback?: PromiseCallback) => {
    return <Promise<Buffer>>promiseCallback(async () => {
        try {
            if (contentId === undefined) throw new Error("Missing contentId");
            let { data } = await api(Token.get()).get(`/content/${contentId}`, downloadOptions(onDownloadProgress) as AxiosRequestConfig);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Updates the content info or content itself using the same FormData structure.
 * @param contentId A Lumicademy content id
 * @param contentData Custom form data for Lumicademy. Use Content.getFormData()
 * @param onUploadProgress (Optional) Progress callback
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ContentData}
 */
export const updateContent = (contentId: string, contentData: FormData, onUploadProgress?: ProgressCallback, callback?: PromiseCallback) => {
    return <Promise<ContentData>>promiseCallback(async () => {
        try {
            if (contentId === undefined) throw new Error("Missing contentId");
            let { data } = await upload(Token.get()).put(`/content/${contentId}`, contentData, fileOptions(onUploadProgress));
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Deletes content with given contentId
 * @param contentId A Lumicademy content id
 * @param forceDelete Force deletion even if content is linked to a conference
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export const deleteContent = (contentId: string, forceDelete?: boolean, callback?: PromiseCallback) => {
    return <Promise<DeleteResult>>promiseCallback(async () => {
        try {
            if (contentId === undefined) throw new Error("Missing contentId");
            let options = forceDelete ? { params: { force: forceDelete } } : {};
            let { data } = await api(Token.get()).delete(`/content/${contentId}`, options);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Returns all contents associated with account
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link ContentData}
 */
export const getContents = (callback?: PromiseCallback) => {
    return <Promise<ContentData[]>>promiseCallback(async () => {
        try {
            let { data } = await api(Token.get()).get("/contents");
            return data.items;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

export default {
    content: {
        new: newContent,
        get: getContent,
        update: updateContent,
        delete: deleteContent
    },
    contents: {
        get: getContents
    }
};
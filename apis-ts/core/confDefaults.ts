export enum STATE {
    WATING = 1,
    STARTED = 2,
    ENDED = 3,
    DELETED = 4
};

export declare interface ConfFeatures {
    video?: boolean,
    audio?: boolean,
    whiteboard?: boolean,
    desktopShare?: boolean,
    documentShare?: boolean,
    fileShare?: boolean,
    recording?: boolean,
    chat?: boolean
};

export declare interface ConfPrivileges {
    annotate?: boolean,
    video?: boolean,
    audio?: boolean,
    share?: boolean,
    viewUsers?: boolean,
    record?: boolean,
    changePages?: boolean,
    changeShares?: boolean,
    chat?: boolean,
    changeUser?: boolean,
    [key: string]: boolean | undefined
};

export declare interface ConfData {
    displayName: string,
    startMethod?: number,
    backgroundColor?: number,
    location?: number,
    countryCode?: string,
    features?: ConfFeatures,
    privileges?: ConfPrivileges,
    presenterPrivileges?: ConfPrivileges,
    hostPrivileges?: ConfPrivileges,
    attendMethods?: {
        password?: boolean,
        url?: boolean
    }
};

export declare interface ConfFilters {
    details: "all" | "summary",
    active?: boolean,
    state?: STATE,
    meta_data1?: string,
    meta_data2?: string,
    meta_data3?: string,
    [key: string]: boolean | STATE | string | undefined
};

export const features: ConfFeatures = {
    video: true,
    audio: true,
    whiteboard: true,
    desktopShare: true,
    documentShare: true,
    fileShare: true,
    recording: true,
    chat: true
};

export const privileges: ConfPrivileges = {
    annotate: true,
    video: true,
    audio: true,
    share: false,
    viewUsers: true,
    record: false,
    changePages: false,
    changeShares: false,
    chat: true,
    changeUser: false
};

export const presenterPrivileges: ConfPrivileges = {
    annotate: true,
    video: true,
    audio: true,
    share: true,
    viewUsers: true,
    record: false,
    changePages: true,
    changeShares: true,
    chat: true,
    changeUser: true
};

export const hostPrivileges: ConfPrivileges = {
    annotate: true,
    video: true,
    audio: true,
    share: true,
    viewUsers: true,
    record: true,
    changePages: true,
    changeShares: true,
    chat: true,
    changeUser: true
};

export const confDefaults: ConfData = {
    displayName: "",
    startMethod: 2,
    backgroundColor: 0,
    location: 1,
    countryCode: "",
    features,
    privileges,
    presenterPrivileges,
    hostPrivileges,
    attendMethods: {
        password: true,
        url: true
    }
}

export default confDefaults;
import { api, promiseCallback, PromiseCallback, DeleteResult, allDetails } from "../config";
import { Token, checkForAuthError } from "../auth";
import confDefaults, { STATE, ConfData, ConfFilters, ConfFeatures } from "./confDefaults";
import { ContentData } from "../content/content-model";

/**
 * Creates a new conference with all default settings and specified confData
 * @param confData User specified conference details. At least displayName required.
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfData}
 */
export const newConference = (confData: ConfData, callback?: PromiseCallback) => {
    return <Promise<ConfData>>promiseCallback(async () => {
        try {
            let { data } = await api(Token.get()).post('/conference', {
                ...confDefaults, ...confData
            });
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Retrieves all details about a given conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfData}
 */
export const getConference = (confId: string, callback?: PromiseCallback) => {
    return <Promise<ConfData>>promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let { data } = await api(Token.get()).get(`/conference/${confId}`, allDetails);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Updates a specified conference with given changes
 * @param confId A Lumicademy conference id
 * @param changes Changes to the conference details
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfData}
 */
export const updateConference = (confId: string, changes: ConfData, callback?: PromiseCallback) => {
    return <Promise<ConfData>>promiseCallback(async () => {
        try {
            if (confId === undefined || changes === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).put(`/conference/${confId}`, changes);
            return data; 
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Deletes a conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing the {@link DeleteResult}
 */
export const deleteConference = (confId: string, callback?: PromiseCallback) => {
    return <Promise<DeleteResult>>promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let { data } = await api(Token.get()).delete(`/conference/${confId}`);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Returns a list of all content currently linked to a given conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link ContentData}
 */
export const getLinkedContent = (confId: string, callback?: PromiseCallback) => {
    return <Promise<ContentData[]>>promiseCallback(async () => {
        try {
            if (confId === undefined) throw new Error("Missing confId");
            let { data } = await api(Token.get()).get(`/conference/${confId}/contents`, allDetails);
            return data.items;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

export enum RelatedAs {
    /** Link to conference as a document for viewing */
    DOCSHARE = 1,
    /** Link to conference as a file for download */
    FILESHARE = 2
};

/**
 * Links content to a specified conference
 * @param confId A Lumicademy conference id
 * @param contentId A Lumicademy content id
 * @param relatedAs (Optional) Relate as a document or file download. Default document
 * @param callback (Optional) Callback function
 * @returns  A promise containing {@link ContentData}
 */
export const linkContent = (confId: string, contentId: string, relatedAs?: RelatedAs, callback?: PromiseCallback) => {
    relatedAs = relatedAs ?? RelatedAs.DOCSHARE;
    return <Promise<ContentData>>promiseCallback(async () => {
        try {
            if (confId === undefined || contentId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).post(`/conference/${confId}/content/${contentId}`, { relatedAs });
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Removes a link between a conference and content. Content is not deleted
 * @param confId A Lumicademy conference id
 * @param contentId A Lumicademy content id
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link DeleteResult}
 */
export const unlinkContent = (confId: string, contentId: string, callback?: PromiseCallback) => {
    return <Promise<DeleteResult>>promiseCallback(async () => {
        try {
            if (confId === undefined || contentId === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).delete(`/conference/${confId}/content/${contentId}`);
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

/**
 * Returns a list of all conferences matching optional filters
 * @param filters (Optional) An object of filters. See {@link ConfFilters}
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link ConfData}
 */
export const getConferences = (filters?: ConfFilters, callback?: PromiseCallback) => {
    return <Promise<ConfData[]>>promiseCallback(async () => {
        try {
            let options: { params: ConfFilters } = { params: { details: "all" } };
            if (filters) {
                Object.keys(filters).forEach(key => {
                    if (filters[key]) options.params[key] = filters[key];
                    options.params[key] = filters[key] ? filters[key] : undefined;
                });
            }
            let { data } = await api(Token.get()).get('/conferences', options);
            return data.items;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

export default {
    conference: {
        new: newConference,
        get: getConference,
        update: updateConference,
        delete: deleteConference,
        content: {
            get: getLinkedContent,
            link: linkContent,
            unlink: unlinkContent
        }
    },
    conferences: {
        get: getConferences
    }
};
import ConferenceAPIs from './conference';
import UserAPIs from './user';
import RecordingAPIs from './recording';
import ToastAPIs from './toast';

export default {
    ...ConferenceAPIs,
    ...UserAPIs,
    ...RecordingAPIs,
    ...ToastAPIs
};
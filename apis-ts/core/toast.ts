import { api, promiseCallback, PromiseCallback } from '../config';
import { Token, checkForAuthError } from '../auth';

export enum ToastIcons {
    None,
    Success,
    Warning,
    Error,
    Info
};

export enum ToastSounds {
    None,
    Chat,
    Join,
    Leave
};

export enum UserGroup {
    Specified = 1,
    All = 2,
    Attendees = 3,
    Presenters = 4,
    Hosts = 5
};

export declare interface ToastData {
    /** The message test to display */
    messageTest: string,
    /** The message icon style. See {@link ToastIcons} */
    icon: number,
    /** The message sound. See {@link ToastSounds} */
    sound: number,
    /** The duration of the message in milliseconds */
    durationMs: number,
    /** The audience of the message. See {@link UserGroup} */
    userGroup: number,
    /** A list of specific user ids to send message to */
    userIds?: Array<string>,
    /** Custom flags for messages */
    flags?: number,
    /** The text to display on an optional button */
    buttonText?: string,
    /** The url to execute when the user presses the custom button */
    buttonUrl?: string
};

declare interface ToastResult {
    result: string
};

const defaultToast = {
    messageTest: "",
    icon: ToastIcons.None,
    sound: ToastSounds.None,
    durationMs: 3000,
    userGroup: UserGroup.Specified,
    userIds: [],
    flags: 0,
    buttonText: undefined,
    buttonUrl: undefined
};

/**
 * Sends a notification to the client app with provided settings
 * @param confId A Lumicademy conference id
 * @param toastData The settings for the notification. See {@link ToastData}
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export const sendToast = (confId: string, toastData: ToastData, callback?: PromiseCallback) => {
    return <Promise<{ result: string }>>promiseCallback(async () => {
        try {
            if (confId === undefined || toastData.messageTest === undefined) throw new Error("Missing arguments");
            let { data } = await api(Token.get()).post(`/conference/${confId}/toast`, {
                ...defaultToast,
                ...toastData
            });
            return data;
        } catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};

export default {
    toast: {
        send: sendToast
    }
};
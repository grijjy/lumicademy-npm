import AuthAPI from './auth';
import CoreAPI from './core';
import ContentAPI from './content';

const Lumi = {
    auth: AuthAPI,
    core: CoreAPI,
    ...ContentAPI
};
export default Lumi;
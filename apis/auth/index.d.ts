/// <reference types="node" />
export declare const TOKEN_KEY = "lumi_access_token";
export declare const REFRESH_TOKEN = "lumi_refresh_token";
export declare const TOKEN_IS_VALID = "lumi_token_is_valid";
export declare interface TokenInfo {
    access_token: string;
    token_type?: string;
    expires_in: number;
    refresh_token?: number;
    grant_type?: string;
    issuer?: string;
    subject?: string;
    scopes?: string;
    client_id?: string;
    storage_id?: string;
    issued_at?: number;
    expires?: number;
}
export declare interface TokenTimerData {
    timer: NodeJS.Timeout | null;
    setup: (tokenInfo: TokenInfo) => void;
    start: (tokenInfo: TokenInfo) => void;
    stop: () => void;
}
export declare type TokenListener = (tokenValid?: Boolean) => void;
export declare interface TokenClass {
    get: () => unknown;
    getRefresh: () => unknown;
    isValid: () => boolean;
    set: (tokenInfo: TokenInfo) => void;
    remove: () => void;
    listeners: TokenListener[];
    addListener: (listener: TokenListener) => void;
    removeListener: (listener: TokenListener) => void;
    notifyListeners: (isValid: boolean) => void;
}
export declare var Token: TokenClass;
/**
 * Checks any error handler for possible authorization issues with Lumicademy.
 * If token is invalid, the token observer is called to notify all subscribers.
 * @param err The error from an error handler
 */
export declare const checkForAuthError: (err: any) => void;
export declare interface SignInProps {
    username: string;
    password: string;
}
declare type SignInCallback = (tokenInfo: TokenInfo) => void;
/**
 * Returns token info from given Lumicademy credentials
 * @param username Lumicademy username
 * @param password Lumicademy password
 * @param callback (Optional) callback function
 * @returns A promise containing {@link TokenInfo}
 */
export declare const signin: ({ username, password }: SignInProps, callback?: SignInCallback | undefined) => Promise<TokenInfo>;
declare type SignOutCallback = ({ result }: {
    result: string;
}) => void;
/**
 * Removes and invalidates the stored token
 * @param access_token (Optional) A specific access_token to log out. Default is stored access_token
 * @param callback (Optional) callback function
 * @returns A promise containing the result
 */
export declare const signout: (access_token?: string | undefined, callback?: SignOutCallback | undefined) => Promise<{
    result: string;
}>;
export declare interface ClientCredentials {
    client_id: string;
    client_secret: string;
}
declare type ClientTokenCallback = (tokenInfo: TokenInfo) => void;
/**
 * Each app under an account has it's own client credentials.
 * Use this function to authorize access to one app instead of an entire account.
 * Find client credentials under the app settings {@link https://developers.lumicademy.com/portal/settings}.
 * @param client_id Lumicademy app client id
 * @param client_secret Lumicademy app client secret
 * @param callback (Optional) callback function
 * @returns A promise containing {@link TokenInfo}
 */
export declare const getTokenFromClientCredentials: ({ client_id, client_secret }: ClientCredentials, callback?: ClientTokenCallback | undefined) => Promise<TokenInfo>;
declare type TokenInfoCallback = (tokenInfo: TokenInfo) => void;
/**
 * Returns details information about the given access_token
 * @param token (Optional) Specific access_token to check
 * @param callback (Optional) callback function
 * @returns A promise containing {@link TokenInfo}
 */
export declare const getTokenInfo: (token?: string | undefined, callback?: TokenInfoCallback | undefined) => Promise<TokenInfo>;
declare type RefreshTokenCallback = (tokenInfo: TokenInfo) => void;
/**
 * Allows the access_token to be refreshed with provided refresh_token
 * @param refresh_token (Optional) A specific refresh_token to use
 * @param callback (Optional) callback function
 * @returns A promise containing {@link TokenInfo}
 */
export declare const refreshToken: (refresh_token?: string | undefined, callback?: RefreshTokenCallback | undefined) => Promise<TokenInfo>;
declare const _default: {
    signin: ({ username, password }: SignInProps, callback?: SignInCallback | undefined) => Promise<TokenInfo>;
    signout: (access_token?: string | undefined, callback?: SignOutCallback | undefined) => Promise<{
        result: string;
    }>;
    token: {
        fromClientCredentials: ({ client_id, client_secret }: ClientCredentials, callback?: ClientTokenCallback | undefined) => Promise<TokenInfo>;
        getInfo: (token?: string | undefined, callback?: TokenInfoCallback | undefined) => Promise<TokenInfo>;
        refresh: (refresh_token?: string | undefined, callback?: RefreshTokenCallback | undefined) => Promise<TokenInfo>;
        get: () => unknown;
        getRefresh: () => unknown;
        isValid: () => boolean;
        set: (tokenInfo: TokenInfo) => void;
        remove: () => void;
        listeners: TokenListener[];
        addListener: (listener: TokenListener) => void;
        removeListener: (listener: TokenListener) => void;
        notifyListeners: (isValid: boolean) => void;
    };
};
export default _default;

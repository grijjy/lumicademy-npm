export declare const allDetails: {
    params: {
        details: string;
    };
};
export declare const summary: {
    params: {
        details: string;
    };
};
export declare type ProgressCallback = (progressEvent: any) => void;
export declare const fileOptions: (uploadCallback?: ProgressCallback | undefined) => {
    headers: {};
    onUploadProgress: ProgressCallback | undefined;
};
export declare const imgOptions: (uploadCallback?: ProgressCallback | undefined) => {
    headers: {
        'content-type': string;
    };
    onUploadProgress: ProgressCallback | undefined;
};
export declare const downloadOptions: (downloadCallback?: ProgressCallback | undefined) => {
    responseType: string;
    onDownloadProgress: ProgressCallback | undefined;
};
/**
 * Creates a new Axios instance for the Lumi API server with optional token
 * @remarks Uses default headers
 * @param token (Optional) access_token to add to Authorization header
 * @returns Uses {@link axios.create} to return a new AxiosInstance
 */
export declare const api: (token?: string | unknown) => import("axios").AxiosInstance;
/**
 * Creates a new Axios instance for the Lumi API server with optional token
 * @remarks Omits default headers for better file handling
 * @param token (Optional) access_token to add to Authorization header
 * @returns Uses {@link axios.create} to return a new AxiosInstance
 */
export declare const upload: (token?: string | unknown) => import("axios").AxiosInstance;
/**
 * Creates a new Axios instance for the Lumi AUTH server with optional token
 * @remarks Uses default headers
 * @param token (Optional) access_token to add to Authorization header
 * @returns Uses {@link axios.create} to return a new AxiosInstance
 */
export declare const auth: (token?: string | unknown) => import("axios").AxiosInstance;
declare type PromiseAction = () => Promise<any>;
export declare type PromiseCallback = (err: any, data: any) => void;
export declare interface DeleteResult {
    result: string;
}
/**
 * Wraps an promise to handle and optional callback
 * @param action An async action {@link PromiseAction}
 * @param callback (Optional) A generic callback {@link PromiseCallback}
 * @returns Either a promise or void if callback is included
 */
export declare const promiseCallback: (action: PromiseAction, callback?: PromiseCallback | undefined) => Promise<any>;
/**
 * Attempts to use cache in localStorage and falls back to
 * node-cache if not running in a browser
 */
export declare const Cache: {
    get: (key: string) => unknown;
    set: (key: string, value: any) => void;
    remove: (key: string) => void;
};
declare const _default: {
    allDetails: {
        params: {
            details: string;
        };
    };
    summary: {
        params: {
            details: string;
        };
    };
    fileOptions: (uploadCallback?: ProgressCallback | undefined) => {
        headers: {};
        onUploadProgress: ProgressCallback | undefined;
    };
    imgOptions: (uploadCallback?: ProgressCallback | undefined) => {
        headers: {
            'content-type': string;
        };
        onUploadProgress: ProgressCallback | undefined;
    };
    downloadOptions: (downloadCallback?: ProgressCallback | undefined) => {
        responseType: string;
        onDownloadProgress: ProgressCallback | undefined;
    };
    api: (token?: unknown) => import("axios").AxiosInstance;
    upload: (token?: unknown) => import("axios").AxiosInstance;
    auth: (token?: unknown) => import("axios").AxiosInstance;
    promiseCallback: (action: PromiseAction, callback?: PromiseCallback | undefined) => Promise<any>;
    Cache: {
        get: (key: string) => unknown;
        set: (key: string, value: any) => void;
        remove: (key: string) => void;
    };
};
export default _default;

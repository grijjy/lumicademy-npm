import axios from 'axios';
import NodeCache from 'node-cache';
const cache = new NodeCache();
;
const defaultHeaders = {
    'accept': 'application/json',
    'content-type': 'application/json'
};
export const allDetails = {
    params: {
        'details': 'all'
    }
};
export const summary = {
    params: {
        'details': 'summary'
    }
};
export const fileOptions = (uploadCallback) => ({
    headers: {},
    onUploadProgress: uploadCallback
});
export const imgOptions = (uploadCallback) => ({
    headers: {
        'content-type': 'image/*'
    },
    onUploadProgress: uploadCallback
});
export const downloadOptions = (downloadCallback) => ({
    responseType: "blob",
    onDownloadProgress: downloadCallback
});
// REQUEST FUNCTIONS
/**
 * Creates a new Axios instance for the Lumi API server with optional token
 * @remarks Uses default headers
 * @param token (Optional) access_token to add to Authorization header
 * @returns Uses {@link axios.create} to return a new AxiosInstance
 */
export const api = (token) => {
    let headers = { ...defaultHeaders };
    if (token) {
        headers['Authorization'] = `Bearer ${token}`;
    }
    return axios.create({
        baseURL: 'https://api.lumicademy.com',
        headers
    });
};
/**
 * Creates a new Axios instance for the Lumi API server with optional token
 * @remarks Omits default headers for better file handling
 * @param token (Optional) access_token to add to Authorization header
 * @returns Uses {@link axios.create} to return a new AxiosInstance
 */
export const upload = (token) => {
    let headers = token
        ? { 'Authorization': `Bearer ${token}` }
        : undefined;
    return axios.create({
        baseURL: 'https://api.lumicademy.com',
        headers
    });
};
/**
 * Creates a new Axios instance for the Lumi AUTH server with optional token
 * @remarks Uses default headers
 * @param token (Optional) access_token to add to Authorization header
 * @returns Uses {@link axios.create} to return a new AxiosInstance
 */
export const auth = (token) => {
    let headers = { ...defaultHeaders };
    if (token) {
        headers['Authorization'] = `Bearer ${token}`;
    }
    return axios.create({
        baseURL: 'https://auth.lumicademy.com',
        headers
    });
};
;
/**
 * Wraps an promise to handle and optional callback
 * @param action An async action {@link PromiseAction}
 * @param callback (Optional) A generic callback {@link PromiseCallback}
 * @returns Either a promise or void if callback is included
 */
export const promiseCallback = async (action, callback) => {
    if (callback) {
        try {
            const result = await action();
            return callback(null, result);
        }
        catch (err) {
            return callback(err, null);
        }
    }
    else {
        return action();
    }
};
/**
 * Attempts to use cache in localStorage and falls back to
 * node-cache if not running in a browser
 */
export const Cache = {
    get: (key) => {
        try {
            return localStorage.getItem(key);
        }
        catch (e) {
            return cache.get(key);
        }
    },
    set: (key, value) => {
        try {
            localStorage.setItem(key, value);
        }
        catch (e) {
            cache.set(key, value);
        }
    },
    remove: (key) => {
        try {
            return localStorage.removeItem(key);
        }
        catch (e) {
            cache.del(key);
            return;
        }
    }
};
export default {
    allDetails,
    summary,
    fileOptions,
    imgOptions,
    downloadOptions,
    api,
    upload,
    auth,
    promiseCallback,
    Cache
};

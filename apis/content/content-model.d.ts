/**
 * Lumicademy Content data structure
 */
export declare interface ContentData {
    /** The Lumicademy content id */
    contentId: string;
    /** The mime type of the content */
    contentType: string;
    /** A display name for the file */
    displayName: string;
    /** The actual name of the file */
    fileName: string;
    /** The delete method for this file. See {@link DeleteMethod} */
    deleteMethod: number;
    /** The date the file was added to Lumicademy */
    created: Date;
}
/**
 * Valid values for content delete method type
 */
export declare enum DeleteMethod {
    /** Never automatically delete content */
    NEVER = 1,
    /** Automatically delete content if parent conference is delete */
    AUTO = 2
}
/**
 * An object for Lumicademy content
 */
export declare class Content {
    /** The Lumicademy content id */
    contentId: string;
    /** The actual content of the file */
    content: any;
    /** The mime type of the content */
    contentType: string;
    /** A display name for the file */
    displayName: string;
    /** The actual name of the file */
    fileName: string;
    /** The date the file was added to Lumicademy */
    created: Date;
    /** The delete method for this file. See {@link DeleteMethod} */
    deleteMethod: number;
    /**
     * Initialize a new Content object from another Content object
     * or from a file input
     * @param content (Optional) Another Content object or file
     */
    constructor(content?: Content | File | any);
    /**
     * Converts a file to a new Content object
     * @param file A file from html input
     * @returns A new Content object
     */
    static fromFile: (file: File) => Content;
    /**
     * A helper function to convert Content object into form data
     * for uploading to Lumicademy
     * @returns Compatible form data for Lumicademy
     */
    getFormData: () => FormData;
}
export default Content;

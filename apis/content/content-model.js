var _a;
;
/**
 * Valid values for content delete method type
 */
export var DeleteMethod;
(function (DeleteMethod) {
    /** Never automatically delete content */
    DeleteMethod[DeleteMethod["NEVER"] = 1] = "NEVER";
    /** Automatically delete content if parent conference is delete */
    DeleteMethod[DeleteMethod["AUTO"] = 2] = "AUTO";
})(DeleteMethod || (DeleteMethod = {}));
;
/**
 * An object for Lumicademy content
 */
export class Content {
    /**
     * Initialize a new Content object from another Content object
     * or from a file input
     * @param content (Optional) Another Content object or file
     */
    constructor(content) {
        /**
         * A helper function to convert Content object into form data
         * for uploading to Lumicademy
         * @returns Compatible form data for Lumicademy
         */
        this.getFormData = () => {
            const request = {
                contentType: this.contentType,
                displayName: this.displayName,
                fileName: this.fileName,
                deleteMethod: this.deleteMethod
            };
            const json = JSON.stringify(request);
            const requestBlob = new Blob([json], { type: "application/json" });
            let data = new FormData();
            data.append('request', requestBlob);
            data.append('content', this.content);
            return data;
        };
        this.contentId = content?.contentId ?? undefined;
        this.content = content?.content ?? content ?? "";
        this.contentType = content?.contentType ?? content?.type ?? "*/*";
        this.displayName = content?.displayName ?? content?.name ?? "";
        this.fileName = content?.fileName ?? content?.name ?? "";
        this.created = content?.created ?? undefined;
        this.deleteMethod = content?.deleteMethod ?? DeleteMethod.NEVER;
    }
}
_a = Content;
/**
 * Converts a file to a new Content object
 * @param file A file from html input
 * @returns A new Content object
 */
Content.fromFile = (file) => {
    return new _a(file);
};
;
export default Content;

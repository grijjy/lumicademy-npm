/// <reference types="node" />
import { ProgressCallback, PromiseCallback, DeleteResult } from "../config";
import { ContentData } from "./content-model";
/**
 * Takes contentData as a FormData object with optional onUploadProgress callback
 * @param contentData Custom form data for Lumicademy. Use Content.getFormData()
 * @param onUploadProgress (Optional) Progress callback
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ContentData}
 */
export declare const newContent: (contentData: FormData, onUploadProgress?: ProgressCallback | undefined, callback?: PromiseCallback | undefined) => Promise<ContentData>;
/**
 * Downloads the content with given contentId.
 * Also has optional onDownloadProgress callback
 * @param contentId A Lumicademy content id
 * @param onDownloadProgress (Optional) Progress callback
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link Buffer}
 */
export declare const getContent: (contentId: string, onDownloadProgress?: ProgressCallback | undefined, callback?: PromiseCallback | undefined) => Promise<Buffer>;
/**
 * Updates the content info or content itself using the same FormData structure.
 * @param contentId A Lumicademy content id
 * @param contentData Custom form data for Lumicademy. Use Content.getFormData()
 * @param onUploadProgress (Optional) Progress callback
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ContentData}
 */
export declare const updateContent: (contentId: string, contentData: FormData, onUploadProgress?: ProgressCallback | undefined, callback?: PromiseCallback | undefined) => Promise<ContentData>;
/**
 * Deletes content with given contentId
 * @param contentId A Lumicademy content id
 * @param forceDelete Force deletion even if content is linked to a conference
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export declare const deleteContent: (contentId: string, forceDelete?: boolean | undefined, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
/**
 * Returns all contents associated with account
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link ContentData}
 */
export declare const getContents: (callback?: PromiseCallback | undefined) => Promise<ContentData[]>;
declare const _default: {
    content: {
        new: (contentData: FormData, onUploadProgress?: ProgressCallback | undefined, callback?: PromiseCallback | undefined) => Promise<ContentData>;
        get: (contentId: string, onDownloadProgress?: ProgressCallback | undefined, callback?: PromiseCallback | undefined) => Promise<Buffer>;
        update: (contentId: string, contentData: FormData, onUploadProgress?: ProgressCallback | undefined, callback?: PromiseCallback | undefined) => Promise<ContentData>;
        delete: (contentId: string, forceDelete?: boolean | undefined, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
    };
    contents: {
        get: (callback?: PromiseCallback | undefined) => Promise<ContentData[]>;
    };
};
export default _default;

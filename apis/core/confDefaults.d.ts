export declare enum STATE {
    WATING = 1,
    STARTED = 2,
    ENDED = 3,
    DELETED = 4
}
export declare interface ConfFeatures {
    video?: boolean;
    audio?: boolean;
    whiteboard?: boolean;
    desktopShare?: boolean;
    documentShare?: boolean;
    fileShare?: boolean;
    recording?: boolean;
    chat?: boolean;
}
export declare interface ConfPrivileges {
    annotate?: boolean;
    video?: boolean;
    audio?: boolean;
    share?: boolean;
    viewUsers?: boolean;
    record?: boolean;
    changePages?: boolean;
    changeShares?: boolean;
    chat?: boolean;
    changeUser?: boolean;
    [key: string]: boolean | undefined;
}
export declare interface ConfData {
    displayName: string;
    startMethod?: number;
    backgroundColor?: number;
    location?: number;
    countryCode?: string;
    features?: ConfFeatures;
    privileges?: ConfPrivileges;
    presenterPrivileges?: ConfPrivileges;
    hostPrivileges?: ConfPrivileges;
    attendMethods?: {
        password?: boolean;
        url?: boolean;
    };
}
export declare interface ConfFilters {
    details: "all" | "summary";
    active?: boolean;
    state?: STATE;
    meta_data1?: string;
    meta_data2?: string;
    meta_data3?: string;
    [key: string]: boolean | STATE | string | undefined;
}
export declare const features: ConfFeatures;
export declare const privileges: ConfPrivileges;
export declare const presenterPrivileges: ConfPrivileges;
export declare const hostPrivileges: ConfPrivileges;
export declare const confDefaults: ConfData;
export default confDefaults;

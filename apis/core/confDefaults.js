export var STATE;
(function (STATE) {
    STATE[STATE["WATING"] = 1] = "WATING";
    STATE[STATE["STARTED"] = 2] = "STARTED";
    STATE[STATE["ENDED"] = 3] = "ENDED";
    STATE[STATE["DELETED"] = 4] = "DELETED";
})(STATE || (STATE = {}));
;
;
;
;
;
export const features = {
    video: true,
    audio: true,
    whiteboard: true,
    desktopShare: true,
    documentShare: true,
    fileShare: true,
    recording: true,
    chat: true
};
export const privileges = {
    annotate: true,
    video: true,
    audio: true,
    share: false,
    viewUsers: true,
    record: false,
    changePages: false,
    changeShares: false,
    chat: true,
    changeUser: false
};
export const presenterPrivileges = {
    annotate: true,
    video: true,
    audio: true,
    share: true,
    viewUsers: true,
    record: false,
    changePages: true,
    changeShares: true,
    chat: true,
    changeUser: true
};
export const hostPrivileges = {
    annotate: true,
    video: true,
    audio: true,
    share: true,
    viewUsers: true,
    record: true,
    changePages: true,
    changeShares: true,
    chat: true,
    changeUser: true
};
export const confDefaults = {
    displayName: "",
    startMethod: 2,
    backgroundColor: 0,
    location: 1,
    countryCode: "",
    features,
    privileges,
    presenterPrivileges,
    hostPrivileges,
    attendMethods: {
        password: true,
        url: true
    }
};
export default confDefaults;

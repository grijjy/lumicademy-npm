import { PromiseCallback, DeleteResult } from "../config";
import { ConfData, ConfFilters } from "./confDefaults";
import { ContentData } from "../content/content-model";
/**
 * Creates a new conference with all default settings and specified confData
 * @param confData User specified conference details. At least displayName required.
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfData}
 */
export declare const newConference: (confData: ConfData, callback?: PromiseCallback | undefined) => Promise<ConfData>;
/**
 * Retrieves all details about a given conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfData}
 */
export declare const getConference: (confId: string, callback?: PromiseCallback | undefined) => Promise<ConfData>;
/**
 * Updates a specified conference with given changes
 * @param confId A Lumicademy conference id
 * @param changes Changes to the conference details
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfData}
 */
export declare const updateConference: (confId: string, changes: ConfData, callback?: PromiseCallback | undefined) => Promise<ConfData>;
/**
 * Deletes a conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing the {@link DeleteResult}
 */
export declare const deleteConference: (confId: string, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
/**
 * Returns a list of all content currently linked to a given conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link ContentData}
 */
export declare const getLinkedContent: (confId: string, callback?: PromiseCallback | undefined) => Promise<ContentData[]>;
export declare enum RelatedAs {
    /** Link to conference as a document for viewing */
    DOCSHARE = 1,
    /** Link to conference as a file for download */
    FILESHARE = 2
}
/**
 * Links content to a specified conference
 * @param confId A Lumicademy conference id
 * @param contentId A Lumicademy content id
 * @param relatedAs (Optional) Relate as a document or file download. Default document
 * @param callback (Optional) Callback function
 * @returns  A promise containing {@link ContentData}
 */
export declare const linkContent: (confId: string, contentId: string, relatedAs?: RelatedAs | undefined, callback?: PromiseCallback | undefined) => Promise<ContentData>;
/**
 * Removes a link between a conference and content. Content is not deleted
 * @param confId A Lumicademy conference id
 * @param contentId A Lumicademy content id
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link DeleteResult}
 */
export declare const unlinkContent: (confId: string, contentId: string, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
/**
 * Returns a list of all conferences matching optional filters
 * @param filters (Optional) An object of filters. See {@link ConfFilters}
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link ConfData}
 */
export declare const getConferences: (filters?: ConfFilters | undefined, callback?: PromiseCallback | undefined) => Promise<ConfData[]>;
declare const _default: {
    conference: {
        new: (confData: ConfData, callback?: PromiseCallback | undefined) => Promise<ConfData>;
        get: (confId: string, callback?: PromiseCallback | undefined) => Promise<ConfData>;
        update: (confId: string, changes: ConfData, callback?: PromiseCallback | undefined) => Promise<ConfData>;
        delete: (confId: string, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
        content: {
            get: (confId: string, callback?: PromiseCallback | undefined) => Promise<ContentData[]>;
            link: (confId: string, contentId: string, relatedAs?: RelatedAs | undefined, callback?: PromiseCallback | undefined) => Promise<ContentData>;
            unlink: (confId: string, contentId: string, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
        };
    };
    conferences: {
        get: (filters?: ConfFilters | undefined, callback?: PromiseCallback | undefined) => Promise<ConfData[]>;
    };
};
export default _default;

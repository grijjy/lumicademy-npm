import { api, promiseCallback, allDetails } from "../config";
import { Token, checkForAuthError } from "../auth";
import confDefaults from "./confDefaults";
/**
 * Creates a new conference with all default settings and specified confData
 * @param confData User specified conference details. At least displayName required.
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfData}
 */
export const newConference = (confData, callback) => {
    return promiseCallback(async () => {
        try {
            let { data } = await api(Token.get()).post('/conference', {
                ...confDefaults, ...confData
            });
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Retrieves all details about a given conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfData}
 */
export const getConference = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing confId");
            let { data } = await api(Token.get()).get(`/conference/${confId}`, allDetails);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Updates a specified conference with given changes
 * @param confId A Lumicademy conference id
 * @param changes Changes to the conference details
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfData}
 */
export const updateConference = (confId, changes, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || changes === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).put(`/conference/${confId}`, changes);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Deletes a conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing the {@link DeleteResult}
 */
export const deleteConference = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing confId");
            let { data } = await api(Token.get()).delete(`/conference/${confId}`);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Returns a list of all content currently linked to a given conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link ContentData}
 */
export const getLinkedContent = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing confId");
            let { data } = await api(Token.get()).get(`/conference/${confId}/contents`, allDetails);
            return data.items;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
export var RelatedAs;
(function (RelatedAs) {
    /** Link to conference as a document for viewing */
    RelatedAs[RelatedAs["DOCSHARE"] = 1] = "DOCSHARE";
    /** Link to conference as a file for download */
    RelatedAs[RelatedAs["FILESHARE"] = 2] = "FILESHARE";
})(RelatedAs || (RelatedAs = {}));
;
/**
 * Links content to a specified conference
 * @param confId A Lumicademy conference id
 * @param contentId A Lumicademy content id
 * @param relatedAs (Optional) Relate as a document or file download. Default document
 * @param callback (Optional) Callback function
 * @returns  A promise containing {@link ContentData}
 */
export const linkContent = (confId, contentId, relatedAs, callback) => {
    relatedAs = relatedAs ?? RelatedAs.DOCSHARE;
    return promiseCallback(async () => {
        try {
            if (confId === undefined || contentId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).post(`/conference/${confId}/content/${contentId}`, { relatedAs });
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Removes a link between a conference and content. Content is not deleted
 * @param confId A Lumicademy conference id
 * @param contentId A Lumicademy content id
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link DeleteResult}
 */
export const unlinkContent = (confId, contentId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || contentId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).delete(`/conference/${confId}/content/${contentId}`);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Returns a list of all conferences matching optional filters
 * @param filters (Optional) An object of filters. See {@link ConfFilters}
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link ConfData}
 */
export const getConferences = (filters, callback) => {
    return promiseCallback(async () => {
        try {
            let options = { params: { details: "all" } };
            if (filters) {
                Object.keys(filters).forEach(key => {
                    if (filters[key])
                        options.params[key] = filters[key];
                    options.params[key] = filters[key] ? filters[key] : undefined;
                });
            }
            let { data } = await api(Token.get()).get('/conferences', options);
            return data.items;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
export default {
    conference: {
        new: newConference,
        get: getConference,
        update: updateConference,
        delete: deleteConference,
        content: {
            get: getLinkedContent,
            link: linkContent,
            unlink: unlinkContent
        }
    },
    conferences: {
        get: getConferences
    }
};

declare const _default: {
    toast: {
        send: (confId: string, toastData: import("./toast").ToastData, callback?: import("../config").PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
    };
    recording: {
        get: (confId: string, recordingId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./recording").RecordingData>;
        share: (confId: string, recordingId: string, settings: import("./recording").ShareOptions, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./recording").ShareResult>;
        download: (confId: string, recordingId: string, filename?: string | undefined, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./recording").ShareResult>;
        updateName: (confId: string, recordingId: string, displayName: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./recording").RecordingData>;
        delete: (confId: string, recordingId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("../config").DeleteResult>;
    };
    recordings: {
        get: (confId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./recording").RecordingData[]>;
    };
    user: {
        new: (confId: string, userData: import("./user").UserData, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./user").User>;
        get: (confId: string, userId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./user").User>;
        update: (confId: string, userId: string, changes: import("./user").UserData, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./user").User>;
        changeRole: (confId: string, userId: string, role?: import("./user").Role | undefined, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./user").User>;
        delete: (confId: string, userId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("../config").DeleteResult>;
        move: (confId: string, userId: string, confDetails: import("./user").MoveData, callback?: import("../config").PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
        expel: (confId: string, userId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
    };
    users: {
        get: (confId: string, filters?: string[] | undefined, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./user").User[]>;
        getTotal: (confId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<number>;
        update: (confId: string, { userIds, userGroup }: import("./user").MultiUserSelector, changes: import("./user").UserData, callback?: import("../config").PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
        changeRole: (confId: string, { userIds, userGroup }: import("./user").MultiUserSelector, role: import("./user").Role, callback?: import("../config").PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
        move: (confId: string, { userIds, userGroup }: import("./user").MultiUserSelector, confDetails: import("./user").MoveData, callback?: import("../config").PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
        expel: (confId: string, { userIds, userGroup }: import("./user").MultiUserSelector, callback?: import("../config").PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
    };
    conference: {
        new: (confData: import("./confDefaults").ConfData, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./confDefaults").ConfData>;
        get: (confId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./confDefaults").ConfData>;
        update: (confId: string, changes: import("./confDefaults").ConfData, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./confDefaults").ConfData>;
        delete: (confId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("../config").DeleteResult>;
        content: {
            get: (confId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("../content/content-model").ContentData[]>;
            link: (confId: string, contentId: string, relatedAs?: import("./conference").RelatedAs | undefined, callback?: import("../config").PromiseCallback | undefined) => Promise<import("../content/content-model").ContentData>;
            unlink: (confId: string, contentId: string, callback?: import("../config").PromiseCallback | undefined) => Promise<import("../config").DeleteResult>;
        };
    };
    conferences: {
        get: (filters?: import("./confDefaults").ConfFilters | undefined, callback?: import("../config").PromiseCallback | undefined) => Promise<import("./confDefaults").ConfData[]>;
    };
};
export default _default;

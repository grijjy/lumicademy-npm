import { PromiseCallback, DeleteResult } from '../config';
export declare interface RecordingData {
    recordingId: string;
    confId: string;
    contentType: string;
    displayName: string;
    filename: string;
    created: Date;
    clientId: string;
    storageId: string;
}
/**
 * Return details about a given recording
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link RecordingData}
 */
export declare const getRecording: (confId: string, recordingId: string, callback?: PromiseCallback | undefined) => Promise<RecordingData>;
/**
 * Returns a list of all recordings of specified conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link RecordingData}
 */
export declare const getRecordings: (confId: string, callback?: PromiseCallback | undefined) => Promise<RecordingData[]>;
export declare interface ShareOptions {
    /** Expiration minutes for public access using the url */
    expiry_minutes?: number;
    /** ISO8601 datetime in UTC format of expiration for public access using the url */
    expiry?: string;
    /** Optional content-disposition such as (attachment; filename=myfilename.mp4) */
    content_disposition?: string;
    /** Optional content-type such as (binary) */
    content_type?: string;
}
export declare interface ShareResult {
    shareAccessUrl: string;
}
/**
 * Returns a shareable url to view recording.
 * See {@link getDownload} for an example of how to use getShareUrl
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param settings Options for share url
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link ShareResult}
 */
export declare const getShareUrl: (confId: string, recordingId: string, settings: ShareOptions, callback?: PromiseCallback | undefined) => Promise<ShareResult>;
/**
 * A specific implementation of {@link getShareUrl} to return a download link with no expiration
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param filename (Optional) Filename for download
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link ShareResult}
 */
export declare const getDownload: (confId: string, recordingId: string, filename?: string | undefined, callback?: PromiseCallback | undefined) => Promise<ShareResult>;
/**
 * Updates the name of the recording
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param displayName A new displayName for the recording
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link RecordingData}
 */
export declare const updateRecordingName: (confId: string, recordingId: string, displayName: string, callback?: PromiseCallback | undefined) => Promise<RecordingData>;
/**
 * Removes a recording
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link DeleteResult}
 */
export declare const deleteRecording: (confId: string, recordingId: string, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
declare const _default: {
    recording: {
        get: (confId: string, recordingId: string, callback?: PromiseCallback | undefined) => Promise<RecordingData>;
        share: (confId: string, recordingId: string, settings: ShareOptions, callback?: PromiseCallback | undefined) => Promise<ShareResult>;
        download: (confId: string, recordingId: string, filename?: string | undefined, callback?: PromiseCallback | undefined) => Promise<ShareResult>;
        updateName: (confId: string, recordingId: string, displayName: string, callback?: PromiseCallback | undefined) => Promise<RecordingData>;
        delete: (confId: string, recordingId: string, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
    };
    recordings: {
        get: (confId: string, callback?: PromiseCallback | undefined) => Promise<RecordingData[]>;
    };
};
export default _default;

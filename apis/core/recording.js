import { api, promiseCallback, allDetails } from '../config';
import { Token, checkForAuthError } from '../auth';
;
/**
 * Return details about a given recording
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link RecordingData}
 */
export const getRecording = (confId, recordingId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || recordingId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).get(`/conference/${confId}/recording/${recordingId}`, allDetails);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Returns a list of all recordings of specified conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of {@link RecordingData}
 */
export const getRecordings = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).get(`/conference/${confId}/recordings`, allDetails);
            return data.items;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
;
;
/**
 * Returns a shareable url to view recording.
 * See {@link getDownload} for an example of how to use getShareUrl
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param settings Options for share url
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link ShareResult}
 */
export const getShareUrl = (confId, recordingId, settings, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || recordingId === undefined)
                throw new Error("Missing arguments");
            let options = {
                params: {
                    details: "all",
                    ...settings
                }
            };
            let { data } = await api(Token.get()).get(`/conference/${confId}/recording/${recordingId}/sharedaccess`, options);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * A specific implementation of {@link getShareUrl} to return a download link with no expiration
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param filename (Optional) Filename for download
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link ShareResult}
 */
export const getDownload = (confId, recordingId, filename, callback) => {
    return promiseCallback(async () => {
        try {
            let settings = {
                content_disposition: `attachment; filename=${filename ?? `Recording_${recordingId}`}.mp4`,
                content_type: "binary"
            };
            return await getShareUrl(confId, recordingId, settings, callback);
        }
        catch (e) {
            throw e;
        }
    }, callback);
};
/**
 * Updates the name of the recording
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param displayName A new displayName for the recording
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link RecordingData}
 */
export const updateRecordingName = (confId, recordingId, displayName, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || recordingId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).put(`/conference/${confId}/recording/${recordingId}`, { displayName });
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Removes a recording
 * @param confId A Lumicademy conference id
 * @param recordingId A Lumicademy recording id
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link DeleteResult}
 */
export const deleteRecording = (confId, recordingId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || recordingId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).delete(`/conference/${confId}/recording/${recordingId}`);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
export default {
    recording: {
        get: getRecording,
        share: getShareUrl,
        download: getDownload,
        updateName: updateRecordingName,
        delete: deleteRecording
    },
    recordings: {
        get: getRecordings
    }
};

import { api, promiseCallback } from '../config';
import { Token, checkForAuthError } from '../auth';
export var ToastIcons;
(function (ToastIcons) {
    ToastIcons[ToastIcons["None"] = 0] = "None";
    ToastIcons[ToastIcons["Success"] = 1] = "Success";
    ToastIcons[ToastIcons["Warning"] = 2] = "Warning";
    ToastIcons[ToastIcons["Error"] = 3] = "Error";
    ToastIcons[ToastIcons["Info"] = 4] = "Info";
})(ToastIcons || (ToastIcons = {}));
;
export var ToastSounds;
(function (ToastSounds) {
    ToastSounds[ToastSounds["None"] = 0] = "None";
    ToastSounds[ToastSounds["Chat"] = 1] = "Chat";
    ToastSounds[ToastSounds["Join"] = 2] = "Join";
    ToastSounds[ToastSounds["Leave"] = 3] = "Leave";
})(ToastSounds || (ToastSounds = {}));
;
export var UserGroup;
(function (UserGroup) {
    UserGroup[UserGroup["Specified"] = 1] = "Specified";
    UserGroup[UserGroup["All"] = 2] = "All";
    UserGroup[UserGroup["Attendees"] = 3] = "Attendees";
    UserGroup[UserGroup["Presenters"] = 4] = "Presenters";
    UserGroup[UserGroup["Hosts"] = 5] = "Hosts";
})(UserGroup || (UserGroup = {}));
;
;
;
const defaultToast = {
    messageTest: "",
    icon: ToastIcons.None,
    sound: ToastSounds.None,
    durationMs: 3000,
    userGroup: UserGroup.Specified,
    userIds: [],
    flags: 0,
    buttonText: undefined,
    buttonUrl: undefined
};
/**
 * Sends a notification to the client app with provided settings
 * @param confId A Lumicademy conference id
 * @param toastData The settings for the notification. See {@link ToastData}
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export const sendToast = (confId, toastData, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || toastData.messageTest === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).post(`/conference/${confId}/toast`, {
                ...defaultToast,
                ...toastData
            });
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
export default {
    toast: {
        send: sendToast
    }
};

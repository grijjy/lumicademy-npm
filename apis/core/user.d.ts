import { PromiseCallback, DeleteResult } from '../config';
import { ConfPrivileges, ConfData } from './confDefaults';
export declare enum Role {
    Attendee = 1,
    Presenter = 2,
    Host = 3
}
export declare enum DeviceState {
    Off = 0,
    On = 1
}
export declare enum AudioAGCMode {
    Unchanged = 0,
    AdaptiveAnalog = 1,
    AdaptiveDigital = 2,
    FixedDigital = 3
}
export declare interface UserData {
    /** Screen name for the conference user */
    displayName?: string;
    privileges?: ConfPrivileges;
    role?: Role;
    /** Set the selected audio playback device id for the active user */
    audioPlaybackDeviceId?: string;
    /** Set the selected audio recording device id for the active user */
    audioRecordingDeviceId?: string;
    /** Set the device hardware mixer recording volume (range 0 to 1) */
    audioRecordingDeviceVolume?: number;
    /** Set the device hardware mixer recording muted state */
    audioRecordingDeviceMuted?: boolean;
    /** The hardware mixer volume (range 0 to 1) for the active user */
    audioPlaybackDeviceVolume?: number;
    /** Set the audio state of the active user. See {@link DeviceState} */
    audioState?: DeviceState;
    /** True to enable the audio automatic gain control for the active user */
    audioAGC?: boolean;
    /** The mode for the automatic gain conrol. See {@link AudioAGCMode} */
    audioAGCMode?: AudioAGCMode;
    audioAGCMicLevelMin?: number;
    audioAGCMicLevelMax?: number;
    audioAGCTargetLevel?: number;
    audioAGCCompressionGain?: number;
    audioAGCEnableLimiter?: boolean;
    /** True to enable the audio noise supression for the active user */
    audioNS?: boolean;
    audioNSAggressiveness?: number;
    /** True to enable the audio acoustic echo cancellation for the active user */
    audioAEC?: boolean;
    audioAECAggressiveness?: number;
    audioAECDelayEstimate?: number;
    audioAECSkewedDelay?: boolean;
    /** Set the selected video capture device id for the active user */
    videoCaptureDeviceId?: string;
    /** Set the video state of the active user. See {@link DeviceState} */
    videoState?: DeviceState;
    /** True to set prefer wide screen video for the active user */
    videoPreferWideScreen?: boolean;
    /** True to set allow high def for the active user */
    videoAllowHD?: boolean;
    /** True to set video limits for the active user */
    videoEnforceLimits?: boolean;
    videoLimits?: {
        maxWidth?: number;
        maxHeight?: number;
        maxFramerate?: number;
    };
    /** A list of users to apply changes to */
    userIds?: string[];
}
export declare enum UserKind {
    Guest = 1,
    Created = 2,
    Account = 3
}
export declare enum UserState {
    Created = 1,
    Deleted = 2
}
export declare interface Device {
    uniqueId: string;
    displayName: string;
}
export declare interface VideoFormat {
    width: number;
    height: number;
    framerate: number;
    minFrameRate: number;
    maxFrameRate: number;
}
export declare interface User {
    userId: string;
    displayName: string;
    kind: UserKind;
    state: UserState;
    attendAsHostUrl: string;
    attendAsPresenterUrl: string;
    attendAsAttendeeUrl: string;
    /** True is the user is currently in the conference */
    active: boolean;
    /** True if the user ever attended */
    attend?: boolean;
    attended?: Date;
    privileges?: ConfPrivileges;
    role?: Role;
    created?: Date;
    deleted?: Date;
    audioPlaybackDevices?: Device[];
    /** The selected audio playback device id for the active user */
    audioPlaybackDeviceId?: string;
    audioRecordingDevices?: Device[];
    /** The selected audio recording device id for the active user */
    audioRecordingDeviceId?: string;
    /** The hardware mixer volume (range 0 to 1) for the active user */
    audioRecordingDeviceVolume?: number;
    audioRecordingDeviceMuted?: boolean;
    /** The hardware mixer volume (range 0 to 1) for the active user */
    audioPlaybackDeviceVolume?: number;
    /** The audio state of the active user. See {@link DeviceState} */
    audioState?: DeviceState;
    /** True if audio automatic gain control is enabled for the active user */
    audioAGC?: boolean;
    /** The mode for the automatic gain conrol. See {@link AudioAGCMode} */
    audioAGCMode?: AudioAGCMode;
    audioAGCMicLevelMin?: number;
    audioAGCMicLevelMax?: number;
    audioAGCTargetLevel?: number;
    audioAGCCompressionGain?: number;
    audioAGCEnableLimiter?: boolean;
    /** True if audio noise supression is enabled for the active user */
    audioNS?: boolean;
    audioNSAggressiveness?: number;
    /** True if audio acoustic echo cancellation is enabled for the active user */
    audioAEC?: boolean;
    audioAECAggressiveness?: number;
    audioAECDelayEstimate?: number;
    audioAECSkewedDelay?: boolean;
    /** Set the selected video capture device id for the active user */
    videoCaptureDeviceId?: string;
    /** The video state of the active user. See {@link DeviceState} */
    videoState?: DeviceState;
    /** An array of video capture formats for the active user */
    videoCaptureFormats?: VideoFormat[];
    /** The selected video capture format for the active user */
    videoCaptureFormat?: VideoFormat;
    /** True if prefer wide screen video is enabled for the active user */
    videoPreferWideScreen?: boolean;
    /** True if allow high def is enabled for the active user */
    videoAllowHD?: boolean;
    /** True if video limits are enabled for the active user */
    videoEnforceLimits?: boolean;
    videoLimits?: {
        maxWidth?: number;
        maxHeight?: number;
        maxFramerate?: number;
    };
}
/**
 * Returns the default privileges for a given role from a conference
 * @param confId A Lumicademy conference id
 * @param role The role to look up privileges for. See {@link Role}
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfPrivileges}
 */
export declare const getPrivileges: (confId: string, role?: Role | undefined, callback?: PromiseCallback | undefined) => Promise<ConfPrivileges>;
/**
 * Creates a new user from provided userData
 * @param confId A Lumicademy conference id
 * @param userData The details for the new user. See {@link UserData}
 * @param callback (Optional) Callback function
 * @returns A promise containing the new {@link User}
 */
export declare const newUser: (confId: string, userData: UserData, callback?: PromiseCallback | undefined) => Promise<User>;
/**
 * Returns details about a given user
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link User}
 */
export declare const getUser: (confId: string, userId: string, callback?: PromiseCallback | undefined) => Promise<User>;
/**
 * Updates a user with provided changes
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param changes Changes to make for the user. See {@link UserData}
 * @param callback (Optional) Callback function
 * @returns A promise containing the updated {@link User}
 */
export declare const updateUser: (confId: string, userId: string, changes: UserData, callback?: PromiseCallback | undefined) => Promise<User>;
/**
 * A quick function to change a user's role and associated privileges
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param role The new {@link Role}
 * @param callback (Optional) Callback function
 * @returns A promise containing updated {@link User}
 */
export declare const changeUserRole: (confId: string, userId: string, role?: Role | undefined, callback?: PromiseCallback | undefined) => Promise<User>;
/**
 * Deletes a user from the conference
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param callback (Optional) Callback function
 * @returns A promise containing the {@link DeleteResult}
 */
export declare const deleteUser: (confId: string, userId: string, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
/**
 * The same as {@link ConfData} with added params
 * for existiing conference options
 */
export declare interface MoveData extends ConfData {
    /** The conference id to use for moving user/s */
    id?: string;
    /** The password to use for moving user/s */
    password?: string;
}
/**
 * Moves the specified user to either an existing or a new conference.
 * @remarks EXISTING: confDetails.id is required and confDetails.password is optional
 * @remarks NEW: confDetails will be the details of the new conference. displayName is required
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param confDetails The details of the new or existing conference
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export declare const moveUser: (confId: string, userId: string, confDetails: MoveData, callback?: PromiseCallback | undefined) => Promise<{
    result: string;
}>;
/**
 * Removes a user from an active conference, but doesn't delete them
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export declare const expelUser: (confId: string, userId: string, callback?: PromiseCallback | undefined) => Promise<{
    result: string;
}>;
export declare enum UserGroupDesc {
    All = "all",
    Attendees = "attendees",
    Presenters = "presenters",
    Hosts = "hosts"
}
export declare interface MultiUserSelector {
    userIds: string[];
    userGroup: UserGroupDesc;
}
export declare interface UserFilters {
    details?: "all" | "summary";
    /** Return only active users */
    active?: boolean;
    /** Return all audio details */
    all_audio?: boolean;
    /** Return all video details */
    all_video?: boolean;
    audio_playback_devices?: boolean;
    audio_playback_device?: boolean;
    audio_recording_devices?: boolean;
    audio_recording_device?: boolean;
    audio_recording_device_volume?: boolean;
    audio_playback_device_volume?: boolean;
    audio_state?: boolean;
    audio_agc?: boolean;
    audio_ns?: boolean;
    audio_aec?: boolean;
    video_capture_devices?: boolean;
    video_capture_device?: boolean;
    video_state?: boolean;
    video_capture_formats?: boolean;
    video_capture_format?: boolean;
    video_prefer_wide_screen?: boolean;
    video_allow_hd?: boolean;
    video_enforce_limits?: boolean;
    video_limits?: boolean;
    [key: string]: string | boolean | undefined;
}
/**
 * Returns all users from a conference with optional filters for details to be returned
 * @param confId A Lumicademy conference id
 * @param filters A list of filters. See {@link UserFilters} for options
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of matching {@link User}
 */
export declare const getUsers: (confId: string, filters?: string[] | undefined, callback?: PromiseCallback | undefined) => Promise<User[]>;
/**
 * Returns the number of users that are currently active in a conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promising containing the total number of active users
 */
export declare const getTotalActiveUsers: (confId: string, callback?: PromiseCallback | undefined) => Promise<number>;
/**
 * Updates either all specified userIds or userGroup with given changes
 * @param confId A Lumicademy conference id
 * @param userIds Lumicademy user ids
 * @param userGroup A Lumicademy user group. See {@link UserGroupDesc}
 * @param changes The changes to make to the users
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export declare const updateUsers: (confId: string, { userIds, userGroup }: MultiUserSelector, changes: UserData, callback?: PromiseCallback | undefined) => Promise<{
    result: string;
}>;
/**
 * A quick function to change specified role and associated privileges of given userIds or userGroup
 * @param confId A Lumicademy conference id
 * @param userIds Lumicademy user ids
 * @param userGroup A Lumicademy user group. See {@link UserGroupDesc}
 * @param role The new role to assign
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export declare const changeUsersRole: (confId: string, { userIds, userGroup }: MultiUserSelector, role: Role, callback?: PromiseCallback | undefined) => Promise<{
    result: string;
}>;
/**
 * Moves the specified users to either an existing or a new conference.
 * @remarks EXISTING: confDetails.id is required and confDetails.password is optional
 * @remarks NEW: confDetails will be the details of the new conference. displayName is required
 * @param confId A Lumicademy conference id
 * @param userIds Lumicademy user ids
 * @param userGroup A Lumicademy user group. See {@link UserGroupDesc}
 * @param confDetails The details of the new or existing conference
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export declare const moveUsers: (confId: string, { userIds, userGroup }: MultiUserSelector, confDetails: MoveData, callback?: PromiseCallback | undefined) => Promise<{
    result: string;
}>;
/**
 * Removes users from an active conference, but doesn't delete them
 * @param confId A Lumicademy conference id
 * @param userIds Lumicademy user ids
 * @param userGroup A Lumicademy user group. See {@link UserGroupDesc}
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export declare const expelUsers: (confId: string, { userIds, userGroup }: MultiUserSelector, callback?: PromiseCallback | undefined) => Promise<{
    result: string;
}>;
declare const _default: {
    user: {
        new: (confId: string, userData: UserData, callback?: PromiseCallback | undefined) => Promise<User>;
        get: (confId: string, userId: string, callback?: PromiseCallback | undefined) => Promise<User>;
        update: (confId: string, userId: string, changes: UserData, callback?: PromiseCallback | undefined) => Promise<User>;
        changeRole: (confId: string, userId: string, role?: Role | undefined, callback?: PromiseCallback | undefined) => Promise<User>;
        delete: (confId: string, userId: string, callback?: PromiseCallback | undefined) => Promise<DeleteResult>;
        move: (confId: string, userId: string, confDetails: MoveData, callback?: PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
        expel: (confId: string, userId: string, callback?: PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
    };
    users: {
        get: (confId: string, filters?: string[] | undefined, callback?: PromiseCallback | undefined) => Promise<User[]>;
        getTotal: (confId: string, callback?: PromiseCallback | undefined) => Promise<number>;
        update: (confId: string, { userIds, userGroup }: MultiUserSelector, changes: UserData, callback?: PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
        changeRole: (confId: string, { userIds, userGroup }: MultiUserSelector, role: Role, callback?: PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
        move: (confId: string, { userIds, userGroup }: MultiUserSelector, confDetails: MoveData, callback?: PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
        expel: (confId: string, { userIds, userGroup }: MultiUserSelector, callback?: PromiseCallback | undefined) => Promise<{
            result: string;
        }>;
    };
};
export default _default;

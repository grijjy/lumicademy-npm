import { api, promiseCallback, allDetails } from '../config';
import { privileges as privilegesDefaults } from './confDefaults';
import { Token, checkForAuthError } from '../auth';
export var Role;
(function (Role) {
    Role[Role["Attendee"] = 1] = "Attendee";
    Role[Role["Presenter"] = 2] = "Presenter";
    Role[Role["Host"] = 3] = "Host";
})(Role || (Role = {}));
;
export var DeviceState;
(function (DeviceState) {
    DeviceState[DeviceState["Off"] = 0] = "Off";
    DeviceState[DeviceState["On"] = 1] = "On";
})(DeviceState || (DeviceState = {}));
;
export var AudioAGCMode;
(function (AudioAGCMode) {
    AudioAGCMode[AudioAGCMode["Unchanged"] = 0] = "Unchanged";
    AudioAGCMode[AudioAGCMode["AdaptiveAnalog"] = 1] = "AdaptiveAnalog";
    AudioAGCMode[AudioAGCMode["AdaptiveDigital"] = 2] = "AdaptiveDigital";
    AudioAGCMode[AudioAGCMode["FixedDigital"] = 3] = "FixedDigital";
})(AudioAGCMode || (AudioAGCMode = {}));
;
;
export var UserKind;
(function (UserKind) {
    UserKind[UserKind["Guest"] = 1] = "Guest";
    UserKind[UserKind["Created"] = 2] = "Created";
    UserKind[UserKind["Account"] = 3] = "Account";
})(UserKind || (UserKind = {}));
;
export var UserState;
(function (UserState) {
    UserState[UserState["Created"] = 1] = "Created";
    UserState[UserState["Deleted"] = 2] = "Deleted";
})(UserState || (UserState = {}));
;
;
;
/**
 * Returns the default privileges for a given role from a conference
 * @param confId A Lumicademy conference id
 * @param role The role to look up privileges for. See {@link Role}
 * @param callback (Optional) Callback function
 * @returns A promise containing {@link ConfPrivileges}
 */
export const getPrivileges = (confId, role, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing confId");
            role = role ?? Role.Attendee;
            let { data } = await api(Token.get()).get(`/conference/${confId}`, allDetails);
            let lookupKey = { 1: "privileges", 2: "presenterPrivileges", 3: "hostPrivileges" };
            let privileges = data[lookupKey[role]];
            if (privileges === undefined) {
                privileges = { ...privilegesDefaults };
                switch (role) {
                    case Role.Attendee:
                        break;
                    case Role.Presenter:
                    case Role.Host:
                        Object.keys(privilegesDefaults).forEach(privilegeKey => {
                            privileges[privilegeKey] = (privilegeKey === "record" && role === Role.Presenter) ? false : true;
                        });
                        break;
                    default:
                        break;
                }
            }
            return privileges;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Creates a new user from provided userData
 * @param confId A Lumicademy conference id
 * @param userData The details for the new user. See {@link UserData}
 * @param callback (Optional) Callback function
 * @returns A promise containing the new {@link User}
 */
export const newUser = (confId, userData, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userData === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).post(`/conference/${confId}/user`, userData);
            if (userData?.role !== undefined && userData?.privileges === undefined) {
                let privileges = await getPrivileges(confId, userData.role);
                return await updateUser(confId, data.userId, { privileges });
            }
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Returns details about a given user
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param callback (Optional) Callback function
 * @returns A promise containing a {@link User}
 */
export const getUser = (confId, userId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).get(`/conference/${confId}/user/${userId}`, allDetails);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Updates a user with provided changes
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param changes Changes to make for the user. See {@link UserData}
 * @param callback (Optional) Callback function
 * @returns A promise containing the updated {@link User}
 */
export const updateUser = (confId, userId, changes, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).put(`/conference/${confId}/user/${userId}`, changes);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * A quick function to change a user's role and associated privileges
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param role The new {@link Role}
 * @param callback (Optional) Callback function
 * @returns A promise containing updated {@link User}
 */
export const changeUserRole = (confId, userId, role, callback) => {
    return promiseCallback(async () => {
        return promiseCallback(async () => {
            try {
                if (confId === undefined || userId === undefined)
                    throw new Error("Missing arguments");
                role = role ?? Role.Attendee;
                let privileges = await getPrivileges(confId, role), user = await updateUser(confId, userId, { role, privileges });
                user.role = role;
                user.privileges = privileges;
                return user;
            }
            catch (e) {
                checkForAuthError(e);
                throw e;
            }
        }, callback);
    }, callback);
};
/**
 * Deletes a user from the conference
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param callback (Optional) Callback function
 * @returns A promise containing the {@link DeleteResult}
 */
export const deleteUser = (confId, userId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).delete(`/conference/${confId}/user/${userId}`);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
;
;
;
/**
 * Moves the specified user to either an existing or a new conference.
 * @remarks EXISTING: confDetails.id is required and confDetails.password is optional
 * @remarks NEW: confDetails will be the details of the new conference. displayName is required
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param confDetails The details of the new or existing conference
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export const moveUser = (confId, userId, confDetails, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined)
                throw new Error("Missing arguments");
            let changes = {}, options = { params: {} };
            if (confDetails.id) {
                options.params.id = confDetails.id;
                if (confDetails.password)
                    options.params.password = confDetails.password;
            }
            else {
                options.params.new = true;
                changes = { ...confDetails };
            }
            let { data } = await api(Token.get()).put(`/conference/${confId}/user/${userId}/move`, changes, options);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Removes a user from an active conference, but doesn't delete them
 * @param confId A Lumicademy conference id
 * @param userId A Lumicademy user id
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export const expelUser = (confId, userId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || userId === undefined)
                throw new Error("Missing arguments");
            let { data } = await api(Token.get()).delete(`/conference/${confId}/user/${userId}/expel`);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
export var UserGroupDesc;
(function (UserGroupDesc) {
    UserGroupDesc["All"] = "all";
    UserGroupDesc["Attendees"] = "attendees";
    UserGroupDesc["Presenters"] = "presenters";
    UserGroupDesc["Hosts"] = "hosts";
})(UserGroupDesc || (UserGroupDesc = {}));
;
;
;
/**
 * Returns all users from a conference with optional filters for details to be returned
 * @param confId A Lumicademy conference id
 * @param filters A list of filters. See {@link UserFilters} for options
 * @param callback (Optional) Callback function
 * @returns A promise containing a list of matching {@link User}
 */
export const getUsers = (confId, filters, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing confId");
            let options = { params: { details: "all" } };
            if (filters) {
                filters.forEach(filter => {
                    options.params[filter] = true;
                });
            }
            let { data } = await api(Token.get()).get(`/conference/${confId}/users`, options);
            return data.items;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Returns the number of users that are currently active in a conference
 * @param confId A Lumicademy conference id
 * @param callback (Optional) Callback function
 * @returns A promising containing the total number of active users
 */
export const getTotalActiveUsers = (confId, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing confId");
            let options = { params: { active: true } }; // Filter for only active users
            let { data } = await api(Token.get()).get(`/conference/${confId}/users`, options);
            return data.items.length;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Updates either all specified userIds or userGroup with given changes
 * @param confId A Lumicademy conference id
 * @param userIds Lumicademy user ids
 * @param userGroup A Lumicademy user group. See {@link UserGroupDesc}
 * @param changes The changes to make to the users
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export const updateUsers = (confId, { userIds, userGroup }, changes, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing confId");
            if (userIds)
                changes.userIds = userIds;
            let options = userGroup ? { params: { apply_to: userGroup } } : {};
            let { data } = await api(Token.get()).put(`/conference/${confId}/users`, changes, options);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * A quick function to change specified role and associated privileges of given userIds or userGroup
 * @param confId A Lumicademy conference id
 * @param userIds Lumicademy user ids
 * @param userGroup A Lumicademy user group. See {@link UserGroupDesc}
 * @param role The new role to assign
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export const changeUsersRole = (confId, { userIds, userGroup }, role, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined || role === undefined)
                throw new Error("Missing arguments");
            let changes = {
                role,
                privileges: await getPrivileges(confId, role)
            };
            let result = await updateUsers(confId, { userIds, userGroup }, changes);
            return result;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
;
;
/**
 * Moves the specified users to either an existing or a new conference.
 * @remarks EXISTING: confDetails.id is required and confDetails.password is optional
 * @remarks NEW: confDetails will be the details of the new conference. displayName is required
 * @param confId A Lumicademy conference id
 * @param userIds Lumicademy user ids
 * @param userGroup A Lumicademy user group. See {@link UserGroupDesc}
 * @param confDetails The details of the new or existing conference
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export const moveUsers = (confId, { userIds, userGroup }, confDetails, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing confId");
            let changes = userIds ? { userIds } : {}, options = userGroup ? { params: { apply_to: userGroup } } : { params: {} };
            if (confDetails.id) {
                options.params.id = confDetails.id;
                if (confDetails.password)
                    options.params.password = confDetails.password;
            }
            else {
                options.params.new = true;
                changes = { ...changes, ...confDetails };
            }
            let { data } = await api(Token.get()).put(`/conference/${confId}/users/move`, changes, options);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
/**
 * Removes users from an active conference, but doesn't delete them
 * @param confId A Lumicademy conference id
 * @param userIds Lumicademy user ids
 * @param userGroup A Lumicademy user group. See {@link UserGroupDesc}
 * @param callback (Optional) Callback function
 * @returns A promise containing the result
 */
export const expelUsers = (confId, { userIds, userGroup }, callback) => {
    return promiseCallback(async () => {
        try {
            if (confId === undefined)
                throw new Error("Missing confId");
            let changes = userIds ? { userIds } : {}, options = userGroup ? { params: { apply_to: userGroup } } : {};
            let { data } = await api(Token.get()).put(`/conference/${confId}/users/expel`, changes, options);
            return data;
        }
        catch (e) {
            checkForAuthError(e);
            throw e;
        }
    }, callback);
};
export default {
    user: {
        new: newUser,
        get: getUser,
        update: updateUser,
        changeRole: changeUserRole,
        delete: deleteUser,
        move: moveUser,
        expel: expelUser
    },
    users: {
        get: getUsers,
        getTotal: getTotalActiveUsers,
        update: updateUsers,
        changeRole: changeUsersRole,
        move: moveUsers,
        expel: expelUsers
    }
};

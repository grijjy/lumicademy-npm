/// <reference types="node" />
declare const Lumi: {
    content: {
        new: (contentData: FormData, onUploadProgress?: import("./config").ProgressCallback | undefined, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./content/content-model").ContentData>;
        get: (contentId: string, onDownloadProgress?: import("./config").ProgressCallback | undefined, callback?: import("./config").PromiseCallback | undefined) => Promise<Buffer>;
        update: (contentId: string, contentData: FormData, onUploadProgress?: import("./config").ProgressCallback | undefined, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./content/content-model").ContentData>;
        delete: (contentId: string, forceDelete?: boolean | undefined, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./config").DeleteResult>;
    };
    contents: {
        get: (callback?: import("./config").PromiseCallback | undefined) => Promise<import("./content/content-model").ContentData[]>;
    };
    auth: {
        signin: ({ username, password }: import("./auth").SignInProps, callback?: ((tokenInfo: import("./auth").TokenInfo) => void) | undefined) => Promise<import("./auth").TokenInfo>;
        signout: (access_token?: string | undefined, callback?: (({ result }: {
            result: string;
        }) => void) | undefined) => Promise<{
            result: string;
        }>;
        token: {
            fromClientCredentials: ({ client_id, client_secret }: import("./auth").ClientCredentials, callback?: ((tokenInfo: import("./auth").TokenInfo) => void) | undefined) => Promise<import("./auth").TokenInfo>;
            getInfo: (token?: string | undefined, callback?: ((tokenInfo: import("./auth").TokenInfo) => void) | undefined) => Promise<import("./auth").TokenInfo>;
            refresh: (refresh_token?: string | undefined, callback?: ((tokenInfo: import("./auth").TokenInfo) => void) | undefined) => Promise<import("./auth").TokenInfo>;
            get: () => unknown;
            getRefresh: () => unknown;
            isValid: () => boolean;
            set: (tokenInfo: import("./auth").TokenInfo) => void;
            remove: () => void;
            listeners: import("./auth").TokenListener[];
            addListener: (listener: import("./auth").TokenListener) => void;
            removeListener: (listener: import("./auth").TokenListener) => void;
            notifyListeners: (isValid: boolean) => void;
        };
    };
    core: {
        toast: {
            send: (confId: string, toastData: import("./core/toast").ToastData, callback?: import("./config").PromiseCallback | undefined) => Promise<{
                result: string;
            }>;
        };
        recording: {
            get: (confId: string, recordingId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/recording").RecordingData>;
            share: (confId: string, recordingId: string, settings: import("./core/recording").ShareOptions, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/recording").ShareResult>;
            download: (confId: string, recordingId: string, filename?: string | undefined, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/recording").ShareResult>;
            updateName: (confId: string, recordingId: string, displayName: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/recording").RecordingData>;
            delete: (confId: string, recordingId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./config").DeleteResult>;
        };
        recordings: {
            get: (confId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/recording").RecordingData[]>;
        };
        user: {
            new: (confId: string, userData: import("./core/user").UserData, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/user").User>;
            get: (confId: string, userId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/user").User>;
            update: (confId: string, userId: string, changes: import("./core/user").UserData, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/user").User>;
            changeRole: (confId: string, userId: string, role?: import("./core/user").Role | undefined, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/user").User>;
            delete: (confId: string, userId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./config").DeleteResult>;
            move: (confId: string, userId: string, confDetails: import("./core/user").MoveData, callback?: import("./config").PromiseCallback | undefined) => Promise<{
                result: string;
            }>;
            expel: (confId: string, userId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<{
                result: string;
            }>;
        };
        users: {
            get: (confId: string, filters?: string[] | undefined, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/user").User[]>;
            getTotal: (confId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<number>;
            update: (confId: string, { userIds, userGroup }: import("./core/user").MultiUserSelector, changes: import("./core/user").UserData, callback?: import("./config").PromiseCallback | undefined) => Promise<{
                result: string;
            }>;
            changeRole: (confId: string, { userIds, userGroup }: import("./core/user").MultiUserSelector, role: import("./core/user").Role, callback?: import("./config").PromiseCallback | undefined) => Promise<{
                result: string;
            }>;
            move: (confId: string, { userIds, userGroup }: import("./core/user").MultiUserSelector, confDetails: import("./core/user").MoveData, callback?: import("./config").PromiseCallback | undefined) => Promise<{
                result: string;
            }>;
            expel: (confId: string, { userIds, userGroup }: import("./core/user").MultiUserSelector, callback?: import("./config").PromiseCallback | undefined) => Promise<{
                result: string;
            }>;
        };
        conference: {
            new: (confData: import("./core/confDefaults").ConfData, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/confDefaults").ConfData>;
            get: (confId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/confDefaults").ConfData>;
            update: (confId: string, changes: import("./core/confDefaults").ConfData, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/confDefaults").ConfData>;
            delete: (confId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./config").DeleteResult>;
            content: {
                get: (confId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./content/content-model").ContentData[]>;
                link: (confId: string, contentId: string, relatedAs?: import("./core/conference").RelatedAs | undefined, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./content/content-model").ContentData>;
                unlink: (confId: string, contentId: string, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./config").DeleteResult>;
            };
        };
        conferences: {
            get: (filters?: import("./core/confDefaults").ConfFilters | undefined, callback?: import("./config").PromiseCallback | undefined) => Promise<import("./core/confDefaults").ConfData[]>;
        };
    };
};
export default Lumi;

import Lumi from './apis/index.js';

/**
 * Uses args from Node script to log in to Lumicademy
 * @returns The token info
 */
const authorize = async () => {
    try {
        if (process.argv[2] === undefined) {
            throw new Error("Please enter username and password arguments separated by spaces after test command");
        } else if (process.argv[3] === undefined) {
            throw new Error("Please enter password after username separated by a space");
        }
        return await Lumi.auth.signin({
            username: process.argv[2],
            password: process.argv[3]
        });
    } catch (e) {
        throw e;
    }
};

/**
 * A wrapper to test any APIs from Lumicademy
 * @returns Any data from APIs
 */
const test = async () => {
    try {
        // Test any apis here
        return "Testing";
    } catch (e) {
        throw e;
    }
};

/**
 * Authorizes the user session, then runs the test
 * @returns Data from the test
 */
const runTest = async () => {
    try {
        await authorize();
        return await test();
    } catch (e) {
        throw e;
    }
};

runTest().then(data => {
    console.log(data);
    process.exit();
}).catch(err => {
    console.error(err);
    process.exit(1);
});
